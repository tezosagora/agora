# SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
#
# SPDX-License-Identifier: AGPL-3.0-or-later

{ haskell-nix }:

let
  project = haskell-nix.stackProject {
    src = with haskell-nix.haskellLib; cleanSourceWith {
      name = "agora";
      src = cleanGit {
        src = ./.;
      };
    };
    modules = [
      ({ pkgs, ... }: {
        packages = {
          agora = {
            components.tests.agora-test = {
              # These are runtime deps, but there is nowhere else to put them
              build-tools = with pkgs; [
                ephemeralpg
              ];
              preCheck = ''
                export TEST_PG_CONN_STRING=$(pg_tmp -w 600)
              '';
              # we need the temporary directory from pg_tmp
              # so extract it out of $TEST_PG_CONN_STRING
              postCheck = ''
                pg_tmp stop -d $(echo ''${TEST_PG_CONN_STRING#*=} | sed 's:%2F:/:g') || :
              '';
            };
          };
        };
      })
    ];
  };
in haskell-nix.haskellLib.selectLocalPackages project
