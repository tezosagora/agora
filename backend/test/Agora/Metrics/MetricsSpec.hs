-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Agora.Metrics.MetricsSpec
      ( spec
      ) where

import Control.Monad.Reader (withReaderT)
import qualified Data.Text as T
import Monad.Capabilities (CapImpl(..), overrideCap)
import Network.HTTP.Types.Status (status502)
import Network.HTTP.Types.Version (http20)
import Servant.Client.Core.Request (RequestF(..))
import Servant.Client.Streaming (BaseUrl(..), ClientError(..), ResponseF(..), Scheme(..))
import Test.Hspec (Spec, describe, it, shouldReturn)
import qualified UnliftIO as UIO

import Agora.Discourse.API
import Agora.Discourse.Client
import Agora.Discourse.Types
import Agora.Metrics.Capability
import Agora.TestMode
import Agora.Util

newtype TestError = TestError Text
  deriving Show

instance Exception TestError

spec :: Spec
spec =
  describe "Metrics infra" $
    it "reports irrecoverable discourse errors" $ do
      tvar <- newTVarIO False
      runWithMockRefs $ \(discourseRef -> discourseIOTVar) -> do
        liftIO $ atomically $ writeTVar discourseIOTVar mockDiscourseRaw
        withReaderT (\c -> overrideCap (mockMetrics tvar) c) $ do
          UIO.withAsync (getDiscourseTopic 10) $ \_ -> waitFor 2
      readTVarIO tvar `shouldReturn` True

  where
    testError = "Dummy Test Error"

    mockMetrics :: MonadIO m => TVar Bool -> CapImpl MetricsCap '[] m
    mockMetrics tvar  = CapImpl $ MetricsCap
      { _sendMetrics  = \case
          Warning msg -> liftIO $ do
            when (testError `T.isInfixOf` msg) $
              atomically $ writeTVar tvar True
          _ -> pure ()
      }

    mockDiscourseRaw :: DiscourseEndpointsIO
    mockDiscourseRaw = DiscourseEndpointsIO
        { dePostTopicIO = \_ _ _ -> pure (CreatedTopic 1 1)
        , deGetCategoryTopicsIO = \_ _ -> pure $ CategoryTopics 0 []
        , deGetCategoriesIO = pure $ CategoryList []
        , deGetTopicIO = \_ -> UIO.throwIO failureResponse
        , deGetPostIO = \_ -> pure $ Post 0 0 ""
        }
      where
        failureResponse = FailureResponse dummyRequest dummyResponse
        dummyRequest = Request @() @(BaseUrl, ByteString) (dummyBaseUrl, mempty) mempty Nothing mempty mempty http20 "GET"
        dummyResponse = Response status502 mempty http20 (encodeUtf8 $ T.pack $ displayException $ TestError testError)
        dummyBaseUrl = BaseUrl Https "dummy" 443 ""
