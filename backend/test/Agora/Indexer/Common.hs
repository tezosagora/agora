-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Agora.Indexer.Common
  ( afp
  , checkExpectationFileOrWrite
  , recordingMockIndexerEndpointsIO
  ) where

import Data.Aeson (FromJSON, ToJSON, decodeFileStrict, eitherDecodeFileStrict)
import Data.Aeson.Encode.Pretty (confCompare, defConfig, encodePretty')
import qualified Data.ByteString.Lazy as BSL
import System.Directory (createDirectoryIfMissing, doesFileExist)
import System.FilePath (takeDirectory, (<.>), (</>))
import Test.Hspec (shouldBe)

import Agora.Indexer.Capability
import qualified Agora.Types as AT

class ToFilePart a where
  toFilePart :: a -> FilePath

instance ToFilePart Text where
  toFilePart = toString

instance ToFilePart (AT.Id a) where
  toFilePart (AT.Id a) = show a

instance ToFilePart (AT.Hash a) where
  toFilePart = toString . AT.hashToText

instance {-# OVERLAPS #-} (Num a, Show a) => ToFilePart a where
  toFilePart = show

instance {-# OVERLAPS #-} (ToFilePart a) => ToFilePart (Maybe a) where
  toFilePart Nothing  = ""
  toFilePart (Just a) = toFilePart a

-- This is a mock for the Indexer that can relay requests to a
-- real indexer, and record the responses obtained from the real indexer to a file.
-- This captures the indexer behavior so that we can later test our code against the
-- recorded indexer behavior. When running tests against the recorded data, we just provide
-- and `undefined` or `error` value for the `realIndexer` argument.
recordingMockIndexerEndpointsIO :: IndexerEndpointsIO ->  IndexerEndpointsIO
recordingMockIndexerEndpointsIO realIndexer = IndexerEndpointsIO
  { neLevel = \lvl@(AT.Level l) s ->
      readRecorded ("levels" </> afp "" l) (neLevel realIndexer lvl s)
  , neHead = readRecorded "head" (neHead realIndexer)
  , neBallotsForProposal = \pHash off lim ->
        readRecorded ("ballots-for-proposal" </> afp "" pHash `afp` off `afp` lim)
          (neBallotsForProposal realIndexer pHash off lim)
  , neBallotsForPeriod = \pid off lim ->
      readRecorded ("ballots-for-period" </> afp "" pid `afp` off `afp` lim)
          (neBallotsForPeriod realIndexer pid off lim)
  , neVoters = \pid off lim ->
      readRecorded ("voters-for-period" </> (afp "" pid `afp` off `afp` lim))
        (neVoters realIndexer pid off lim)
  , neBlockCount = readRecorded "block-count" (neBlockCount realIndexer)
  , neGetProposal = \phash ->
      readRecorded ("proposals" </> afp "" phash) (neGetProposal realIndexer phash)
  , neGetProposals = \epochId -> do
      readRecorded ("proposals-by-epoch" </> afp "" epochId) (neGetProposals realIndexer epochId)
  , neProposalOperation = \pid off lim ->
      readRecorded ("proposals-operation-for-proposal" </>
        afp "" pid `afp` off `afp` lim) (neProposalOperation realIndexer pid off lim)
  , neProposalOperationsForPeriod = \pid off lim ->
      readRecorded ("proposals-operation-for-proposal" </>
        (afp "" pid `afp` off `afp` lim)) (neProposalOperationsForPeriod realIndexer pid off lim)
  , neProtocols = \off lim ->
      readRecorded ("protocols" </>  (afp "protocols" off `afp` lim)) (neProtocols realIndexer off lim)
  , neEpoch = \epochId ->  readRecorded ("epoch" </> afp "" epochId) (neEpoch realIndexer epochId)
  , neEpochs = readRecorded "epochs" (neEpochs realIndexer)
  }
  where

    testFilesDir :: FilePath
    testFilesDir = "resources/indexer/test-data"

    readRecorded :: (ToJSON a, FromJSON a) => FilePath -> IO a -> IO a
    readRecorded fpin real =
      doesFileExist fp >>= \case
        True -> do
          eitherDecodeFileStrict fp >>= \case
            Right a  -> pure a
            Left err -> error (toText err)
        False -> do
          a <- real
          encodeFileCreatingDir fp a
          pure a
      where
        fp = testFilesDir </> fpin <.> "json"

afp :: ToFilePart a => FilePath -> a -> FilePath
afp "" a     = toFilePart a
afp prefix a = prefix <> "-" <> toFilePart a

encodeFileCreatingDir :: ToJSON a => FilePath -> a -> IO ()
encodeFileCreatingDir fp a = do
  createDirectoryIfMissing True (takeDirectory fp)
  BSL.writeFile fp (encodePretty' (defConfig { confCompare = compare }) a)

-- To make this function update the expectation, do the following at the
-- begining of the function.
--    encodeFileCreatingDir expectationFile real
checkExpectationFileOrWrite :: (Eq a, Show a, ToJSON a, FromJSON a) => FilePath -> a -> IO ()
checkExpectationFileOrWrite expectationFile real = do
  expected <- fromMaybe (error "error decoding expectation") <$> decodeFileStrict expectationFile
  real `shouldBe` expected
