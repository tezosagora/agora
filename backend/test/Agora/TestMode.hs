-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE OverloadedLabels #-}

{-|
Defines the monadic stack and parameters for testing monadic Agora code.
-}
module Agora.TestMode
  ( WaiUrls
  , MockRefs(..)
  , invalidWaiUrls
  , emptyDiscourseEndpoints
  , proposalHashes
  , runWithMocks
  , runWithMockRefs
  , runWithMocksReturnDiscourse
  , testDiscourseCategory
  , testingConfig
  ) where

import Prelude hiding (ByteString)

import Control.Monad.Reader (withReaderT)
import Data.Aeson (decodeFileStrict)
import qualified Data.List.NonEmpty as NE
import qualified Data.Map as Map
import Lens.Micro.Platform ((?~))
import Loot.Log
import Monad.Capabilities (CapImpl(..), CapsT, addCap, emptyCaps)

import Servant.Client (BaseUrl(..), Scheme(..))
import Servant.Client.Generic (AsClientT)

import Agora.BakerFetch.Api
import Agora.BakerFetch.Types
import Agora.Config
import Agora.Discourse
import Agora.Indexer.Capability
import Agora.Indexer.Common (recordingMockIndexerEndpointsIO)
import Agora.Metrics.Capability
import Agora.Mode
import Agora.State.Capability
import Agora.Types

-- The type hold the references to the mock impmementations of some
-- capabilities that can be swapped out during a tests. We need this becasue of
-- the pattern where we create threads from `with*` capability implementation
-- function, and the capabilities accessed by this thread cannot be replaced
-- with a different implementation from the test, because the thread creation
-- happens at the unmodified reader environment. But using these TVars, which
-- would be looked each time by the mock implementation, we can swap out any
-- of these services by writing to them a different set of mock methods.
data MockRefs = MockRefs
  { discourseRef :: TVar DiscourseEndpointsIO
  , metricsRef :: TVar (MetricsCap IO)
  , bakerFetchFef :: TVar BakerFetchEndpointsIO
  }

runWithMocks
  :: CapsT AgoraCaps Base a
  -> IO a
runWithMocks action = do
  (a, _, _) <- runWithMocksReturnDiscourse Nothing $ \_ -> do
    idxStartCacheRefresh
    idxWaitForCacheRefresh 3
    action
  pure a

runWithMockRefs
  :: (MockRefs -> CapsT AgoraCaps Base a)
  -> IO a
runWithMockRefs action = do
  (a, _, _) <- runWithMocksReturnDiscourse Nothing $ \tvars -> do
    action tvars
  pure a

-- This function runs an action in AgoraCaps capability in the Agora environment
-- using mocks. It returns the result of the action, along with the list of discourse
-- topics and posts created.
--
-- The recordingMockIndexerEndpointsIO can run a mock indexed using previously recorded
-- data, if it is available, or else it will invoke the actions in the `IndexerEndpointsIO`
-- argument. The recorded data can be updated by deleting existing data, and running this test
-- with a real indexer, possibly running locally. It can be obtained as follows.
--
-- The refresh inteval needs to be around 10 seconds because the fetching data from real indexer is
-- slower then fetching it from files.
--
--    bUrl <- parseBaseUrl "http://localhost:5000/v1"
--    env <- getClientEnv bUrl
--    realIndexer <- getIndexerReal env
--    ...
--    ...
--    withIndexer (recordingMockIndexerEndpointsIO realIndexer) 10 $ \_ -> do
--      idxWaitForCacheRefresh 2
--      action
--

runWithMocksReturnDiscourse
  :: Maybe IndexerEndpointsIO
  -> (MockRefs -> CapsT AgoraCaps Base a)
  -> IO (a, [Topic], [Post])
runWithMocksReturnDiscourse midxEp action = runWithMocksReturnDiscourseImpl (fromMaybe (recordingMockIndexerEndpointsIO (error "unavailable")) midxEp) $ \tvars -> do
    idxStartCacheRefresh
    idxWaitForCacheRefresh 3
      -- We need at least 3 refresh cycles for proposals to get created at discourse and have them linked in the cache
      -- On the first refresh, the proposals are lined up to be matched with proposals.
      -- On the second run, unmatched proposals trigger proposal creation calls.
      -- On the third run, the proposal topics created in the above step are linked with the corresponding proposals.
    action tvars

runWithMocksReturnDiscourseImpl
  :: IndexerEndpointsIO
  -> (MockRefs -> CapsT AgoraCaps Base a)
  -> IO (a, [Topic], [Post])
runWithMocksReturnDiscourseImpl idxEp action = do
 (discourseIOMock, topicsVar, postsVar, discourseEpIOTVar) <- mockDiscourseIO
 (mockMetricsCapImp, metricsCapTVar) <- mockMetrics
 (mockBakerFetchCapImp, bakerFetchCapTVar) <- mockBakerFetch
 r <- usingReaderT emptyCaps $

     withStateCap $
     withReaderT (addCap mockLogging) $
     withReaderT (addCap mockConfig) $
     withReaderT (addCap mockMetricsCapImp) $
     withDiscourseClient discourseIOMock $
     withReaderT (addCap mockBakerFetchCapImp) $

     withIndexer idxEp 0 $ do
       action (MockRefs discourseEpIOTVar metricsCapTVar bakerFetchCapTVar)

 createdPosts <- readTVarIO postsVar
 createdTopics <- readTVarIO topicsVar
 pure (r, Map.elems createdTopics, Map.elems createdPosts)

  where

    mockLogging :: MonadIO m => CapImpl Logging '[] m
    mockLogging  = CapImpl $ Logging
      { _log  = \_-> pass -- putTextLn $ decodeUtf8 $ fmtMessageFlat e -- (uncomment to enable logs during tests)
      , _logName  = pure $ GivenName "Dummy"
      }

    -- A mock implementation that looks up methods in a TVar and executes
    -- them. Enables swapping in a different implementation.
    mockMetrics :: IO (CapImpl MetricsCap '[] IO, TVar (MetricsCap IO))
    mockMetrics  = do
      metricsCapTvar <- newTVarIO mockMetricsCap
      pure (CapImpl $ MetricsCap
        { _sendMetrics = \me -> do
            metricsCap <- readTVarIO metricsCapTvar
            liftIO $ _sendMetrics metricsCap me
        }, metricsCapTvar)

    mockMetricsCap = MetricsCap
      { _sendMetrics = \_ -> pure ()
      }

    mockBakerFetch :: IO (CapImpl BakerFetchCap BakerFetchDeps IO, TVar BakerFetchEndpointsIO)
    mockBakerFetch  = do
      bakerFetchCapTvar <- newTVarIO $ BakerFetchEndpointsIO
        { bfGetBakers = fromMaybe (error "Error decoding bakers.json") <$> decodeFileStrict "resources/indexer/bakers.json"
        }
      pure (bakerFetchCap $ BakerFetchEndpointsIO
        { bfGetBakers = do
            bakerFetchIO <- readTVarIO bakerFetchCapTvar
            bfGetBakers bakerFetchIO
        }, bakerFetchCapTvar)

    mockConfig :: Monad m => CapImpl AgoraConfigCap '[] m
    mockConfig  = newConfig (testingConfig invalidWaiUrls)

mockDiscourseIO :: IO (DiscourseEndpointsIO, TVar (Map.Map DiscourseTopicId Topic), TVar (Map.Map DiscoursePostId Post), TVar DiscourseEndpointsIO)
mockDiscourseIO = do
  topics <- newTVarIO $ Map.fromList [(existingTopicId, existingTopic)]
  posts <- newTVarIO mempty
  let d = defaultMockDiscourseIO topics posts
  discourceEpTvar <- newTVarIO d
  pure (mockDiscourseIO' discourceEpTvar, topics, posts, discourceEpTvar)

  where
    mockDiscourseIO' :: TVar DiscourseEndpointsIO -> DiscourseEndpointsIO
    mockDiscourseIO' tvar = DiscourseEndpointsIO
      { dePostTopicIO  = \a b ct -> do
          DiscourseEndpointsIO {..} <- liftIO $ readTVarIO tvar
          dePostTopicIO a b ct
      , deGetCategoryTopicsIO = \cid a -> do
          DiscourseEndpointsIO {..} <- liftIO $ readTVarIO tvar
          deGetCategoryTopicsIO cid a
      , deGetCategoriesIO = do
          DiscourseEndpointsIO {..} <- liftIO $ readTVarIO tvar
          deGetCategoriesIO
      , deGetPostIO = \postId -> do
          DiscourseEndpointsIO {..} <- liftIO $ readTVarIO tvar
          deGetPostIO postId
      , deGetTopicIO = \topicId -> do
          DiscourseEndpointsIO {..} <- liftIO $ readTVarIO tvar
          deGetTopicIO topicId
      }

    existingTopicId = 1000
    existingTopic = MkTopic existingTopicId (Title "(PsDELPH1K) Manually made") (NE.fromList [existingPost])
    existingPost = Post 1010 existingTopicId "Some manually made post"

    defaultMockDiscourseIO
      :: TVar (Map.Map DiscourseTopicId Topic)
      -> TVar (Map.Map DiscoursePostId Post)
      -> DiscourseEndpointsIO
    defaultMockDiscourseIO topics posts = do
      DiscourseEndpointsIO
        { dePostTopicIO  = \_ _ ct -> mkNewTopic ct
        , deGetCategoryTopicsIO = \cid _ ->
            if cid == 11
              then getTopics
              else error "Unexpected category id"
        , deGetCategoriesIO = pure $ CategoryList [Category 10 "SomethingElse", Category 11 "Proposals"]
        , deGetPostIO = \postId -> do
            mPosts <- readTVarIO posts
            pure $ fromMaybe (error $ "Post not found for" <> show postId) $ Map.lookup postId mPosts
        , deGetTopicIO = \topicId -> do
            mTopics <- readTVarIO topics
            pure $ fromMaybe (error $ "Topic not found" <> show topicId) $ Map.lookup topicId mTopics
        }
      where
        getTopics :: IO CategoryTopics
        getTopics = do
          topics_ <- Map.elems <$> readTVarIO topics
          pure $ CategoryTopics 100 $ (\t -> TopicHead (tId t) (tTitle t)) <$> topics_

        mkNewTopic :: CreateTopic -> IO CreatedTopic
        mkNewTopic CreateTopic {..} = do
          mTopics <- readTVarIO topics
          case find (\topic -> tTitle topic == ctTitle) (Map.elems mTopics) of
            Just _existingTopic ->
              error $ "Topic with title " <> unTitle ctTitle <> " already exists"
            Nothing -> do
              newTopicId <- getNewTopicId
              newPostId <- getNewPostId
              let newPost = Post newPostId newTopicId (unRawBody ctRaw)
              let newTopic = MkTopic newTopicId ctTitle (NE.fromList [newPost])
              atomically $ modifyTVar' posts (Map.insert newPostId newPost)
              atomically $ modifyTVar' topics (Map.insert newTopicId newTopic)
              pure $ CreatedTopic newPostId newTopicId

        getNewTopicId :: IO DiscourseTopicId
        getNewTopicId = do
          keys_ <- Map.keys <$> readTVarIO topics
          pure $ maybe 0 ((+1) . maximum) (nonEmpty keys_)

        getNewPostId :: IO DiscoursePostId
        getNewPostId = do
          keys_ <- Map.keys <$> readTVarIO posts
          pure $ maybe 0 ((+1) . maximum) (nonEmpty keys_)

data WaiUrls = WaiUrls
  { discourseUrl  :: BaseUrl
  , bakerFetchUrl :: BaseUrl
  , indexerUrl    :: BaseUrl
  } deriving Show

invalidWaiUrls :: WaiUrls
invalidWaiUrls = WaiUrls
  { discourseUrl = BaseUrl Https "tezos.discourse.invalid" 443 ""
  , bakerFetchUrl = BaseUrl Https "tezos.baker.invalid" 443 ""
  , indexerUrl = BaseUrl Https "indexer.invalid" 443 ""
  }

emptyDiscourseEndpoints :: DiscourseEndpoints (AsClientT m)
emptyDiscourseEndpoints = DiscourseEndpoints
  { dePostTopic = error "dePostTopic isn't supposed to be called"
  , deGetCategoryTopics = error "deGetCategoryTopics isn't supposed to be called"
  , deGetCategories = error "deGetCategories isn't supposed to be called"
  , deGetTopic = error "deGetTopic isn't supposed to be called"
  , deGetPost = error "deGetPost isn't supposed to be called"
  }

testDiscourseCategory :: Text
testDiscourseCategory = "Proposals"

-- | Configuration which is used in tests. Accepts a `ConnString`
-- which is determined at runtime.
testingConfig :: WaiUrls -> AgoraConfigRec
testingConfig urls = finaliseDeferredUnsafe $ mempty
  & option #logging ?~ basicConfig
  & option #bakerfetch_url ?~ bakerFetchUrl urls
  & option #indexer_url ?~ indexerUrl urls
  & sub #discourse . option #host ?~ discourseUrl urls
  & sub #discourse . option #category ?~ testDiscourseCategory
  & option #predefined_bakers  ?~
      [ BakerInfo "Foundation Baker 1" (encodeHash "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9") Nothing Nothing
      , BakerInfo "Foundation Baker 2" (encodeHash "tz3bvNMQ95vfAYtG8193ymshqjSvmxiCUuR5") Nothing Nothing
      ]
  & option #network_history  ?~
      Map.fromList
        [ ("periodsPerEpoch", HistoricValue 5 [LevelRangedValue 0 1343488 4]) ]

proposalHashes :: [ProposalHash]
proposalHashes = encodeHash <$>
      [ "Psd1ynUBhMZAeajwcZJAeq5NrxorM6UCU4GJqxZ7Bx2e9vUWB6z"
      , "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
      , "PtdRxBHvc91c2ea2evV6wkoqnzW7TadTg9aqS9jAn2GbcPGtumD"
      , "PsBABY5nk4JhdEv1N1pZbt6m6ccB9BfNqa23iKZcHBh23jmRS9f"
      , "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU"
      , "PtCarthavAMoXqbjBPVgDCRd5LgT7qqKWUPXnYii3xCaHRBMfHH"
      , "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"
      , "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo"
      , "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq"
      , "PsFLorBArSaXjuy9oP76Qv1v2FRYnUs7TFtteK5GkRBC24JvbdE"
      , "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
      , "Psd1ynUBhMZAeajwcZJAeq5NrxorM6UCU4GJqxZ7Bx2e9vUWB6z"
      , "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
      , "PtdRxBHvc91c2ea2evV6wkoqnzW7TadTg9aqS9jAn2GbcPGtumD"
      , "PsBABY5nk4JhdEv1N1pZbt6m6ccB9BfNqa23iKZcHBh23jmRS9f"
      , "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU"
      , "PtCarthavAMoXqbjBPVgDCRd5LgT7qqKWUPXnYii3xCaHRBMfHH"
      , "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"
      , "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo"
      , "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq"
      , "PsFLorBArSaXjuy9oP76Qv1v2FRYnUs7TFtteK5GkRBC24JvbdE"
      , "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
      , "Psd1ynUBhMZAeajwcZJAeq5NrxorM6UCU4GJqxZ7Bx2e9vUWB6z"
      , "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
      , "PtdRxBHvc91c2ea2evV6wkoqnzW7TadTg9aqS9jAn2GbcPGtumD"
      , "PsBABY5nk4JhdEv1N1pZbt6m6ccB9BfNqa23iKZcHBh23jmRS9f"
      , "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU"
      , "PtCarthavAMoXqbjBPVgDCRd5LgT7qqKWUPXnYii3xCaHRBMfHH"
      , "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"
      , "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo"
      , "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq"
      , "PsFLorBArSaXjuy9oP76Qv1v2FRYnUs7TFtteK5GkRBC24JvbdE"
      , "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"]
