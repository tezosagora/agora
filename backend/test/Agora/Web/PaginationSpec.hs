-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# OPTIONS_GHC -Wno-orphans #-}

module Agora.Web.PaginationSpec
  ( spec
  ) where

import Test.Hspec (Spec, describe, it, shouldBe)

import Agora.Pagination (Limit (..), PaginatedList (..), PaginationData (..), buildPaginatedList)
import Agora.Types (Id (..))
import Agora.Util (HasId (..))

data MockTag = MockTag
  deriving (Show, Ord, Eq)

data MockData = MockData
  { _mdId :: Id MockTag
  , _mdField :: Int
  } deriving (Show, Ord, Eq)

instance HasId MockData where
  type IdT MockData  = Id MockTag
  type TagT MockData = MockTag
  getId = _mdId

spec :: Spec
spec = describe "Pagination" $ do
  let mockDataList = [MockData (Id $ fromIntegral i) (100 - i) | i <- [1..100]]
  it "Does partition correctly in presence of non-unique field values" $ do
    let
      mockDataListNonUniq = [MockData (Id $ fromIntegral i) f | (i, f) <- [(10::Int, 2), (20, 3), (30, 2), (40, 3), (50, 3)]]
      expected = PaginatedList
        { plPagination = PaginationData { pdRest = 0, pdLimit = Nothing, pdLastId = Just 30 }
        , plResults = uncurry MockData <$> [(40, 3), (50, 3), (10, 2), (30, 2)]
        }
      actual = buildPaginatedList Nothing (Just 20) _mdId _mdField mockDataListNonUniq
    actual `shouldBe` expected
  it "Doesn't do paging without specifying offset and limit" $ do
    let
      expected = PaginatedList
        { plPagination = PaginationData { pdRest = 0, pdLimit = Nothing, pdLastId = Just 1 }
        , plResults = sortOn (Down . _mdId) mockDataList
        }
      actual = buildPaginatedList Nothing Nothing _mdId _mdId mockDataList
    actual `shouldBe` expected
  it "Returns the full list when limit is greater than data size" $ do
    let
      limit = Just $ Limit $ 100 + (fromIntegral @Int @Word32 $ length mockDataList)
      expected = PaginatedList
        { plPagination = PaginationData { pdRest = 0, pdLimit = limit, pdLastId = Just 1 }
        , plResults = sortOn (Down . _mdId) mockDataList
        }
      actual = buildPaginatedList limit Nothing _mdId _mdId mockDataList
    actual `shouldBe` expected
  it "Returns empty list when offset is greater than data size" $ do
    let
      lastId = Just $ Id 0
      expected = PaginatedList
        { plPagination = PaginationData { pdRest = 0, pdLimit = Nothing, pdLastId = Nothing }
        , plResults = []
        }
      actual = buildPaginatedList Nothing lastId _mdId _mdId mockDataList
    actual `shouldBe` expected
  it "Can do sorting not only by IDs" $ do
    let
      expected = PaginatedList
        { plPagination = PaginationData { pdRest = 0, pdLimit = Nothing, pdLastId = Just 100 }
        , plResults = sortOn (Down . _mdField) mockDataList
        }
      actual = buildPaginatedList Nothing Nothing _mdField _mdField mockDataList
    actual `shouldBe` expected
  it "Does paging correctly when both offset and limit are specified" $ do
    let
      limit = Just $ Limit 20
      lastId = Just $ Id 61
      expected = PaginatedList
        { plPagination = PaginationData { pdRest = 60, pdLimit = limit, pdLastId = lastId }
        , plResults = take 20 $ drop 20 $ sortOn (Down . _mdId) mockDataList
        }
      actual = buildPaginatedList limit (Just $ Id 81) _mdId _mdId mockDataList
    expected `shouldBe` actual
