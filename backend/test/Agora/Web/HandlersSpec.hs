-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Agora.Web.HandlersSpec
  ( spec
  ) where

import Test.Hspec (Expectation, Spec, describe, it, shouldBe, shouldSatisfy)

import Agora.Pagination (plResultsL)
import Agora.Web.Handlers
import Agora.Web.Types
import Agora.TestMode
import Agora.Types

spec :: Spec
spec = describe "Web handlers" $ do
  let periodId = 43
  describe "getSpecificProposalVotes" $ do
    it "Specific proposal votes are sorted in the descending order by id" $ do
      let proposalHashText = "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
      pagedRes <- runWithMocks $ getSpecificProposalVotes proposalHashText periodId Nothing Nothing
      checkSortedBy (pagedRes ^. plResultsL) (Down . _pvId)
  describe "getProposalVotes" $ do
    it "Proposal votes are sorted in the descending order by id" $ do
      pagedRes <- runWithMocks $ getProposalVotes periodId Nothing Nothing
      checkSortedBy (pagedRes ^. plResultsL) (Down . _pvId)
  describe "getBallots" $ do
    it "Ballots are sorted in the descending order by timestamp" $ do
      pagedRes <- runWithMocks $ getBallots periodId Nothing Nothing Nothing
      checkSortedBy (pagedRes ^. plResultsL) (Down . view bTimestamp)
    it "Ballots are correctly filtered by decision" $ do
      let allDecisions :: [Decision] = [minBound .. maxBound]
      for_ allDecisions $ \decision -> do
        pagedRes <- runWithMocks $ getBallots periodId Nothing Nothing $ Just [decision]
        let res = pagedRes ^. plResultsL
        res `shouldSatisfy` all ((== decision) . _bDecision)
  describe "getNonVoters" $ do
    it "Non voters are sorted in the descending order by voting power" $ do
      res <- runWithMocks $ getNonVoters periodId
      checkSortedBy res (Down . _bkVotingPower)
  where
    checkSortedBy :: (Show a, Eq a, Ord b) => [a] -> (a -> b) -> Expectation
    checkSortedBy xs fn = xs `shouldBe` sortOn fn xs
