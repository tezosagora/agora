-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Agora.Pagination
  ( Limit (..)
  , PaginationData (..)
  , PaginationDataTag
  , PaginatedList (..)
  , buildPaginatedList
  , plResultsL
  ) where

import Data.Aeson (ToJSON)
import Data.List qualified as DL
import Data.Aeson.TH (deriveJSON)
import qualified Data.List.NonEmpty as NE
import Fmt (Buildable(..), (+|), (|+))
import Lens.Micro.Platform (makeLensesFor)
import Servant.API (FromHttpApiData(..))
import Servant.Util (ForResponseLog(..), buildListForResponse)

import Agora.Types (Id (..))
import Agora.Util (Amount, HasId(..), IdT, buildFromJSON)
import Agora.Util.JsonOptions (defaultOptions)

-- | Dummy datatype to parameterize Id in 'PaginatedData' so not
-- to parameterize 'PaginatedData' itself.
data PaginationDataTag

coerceToPaginationId :: Id a -> Id PaginationDataTag
coerceToPaginationId (Id x) = Id x

-- | Datatype which represents pagination parameters which are returned
-- alongside the paginated data.
data PaginationData = PaginationData
  { pdRest   :: !Amount
  , pdLimit  :: !(Maybe Limit)
  , pdLastId :: !(Maybe (Id PaginationDataTag))
  } deriving (Show, Eq, Generic)

-- | Object which represents a paginated list of results with metadata.
data PaginatedList a = PaginatedList
  { plPagination :: !PaginationData
  , plResults    :: ![a]
  } deriving (Show, Eq, Generic)

newtype Limit = Limit Word32
  deriving (Eq, Ord, Show, Generic, Num, Real, Integral, Enum, FromHttpApiData, Buildable)

-- | Build a Paginated list. Sorts the items in descending order by the given
-- field using the  provided function to get this field. Another function can
-- be provided for partitioning values that are already sent. This value extracted
-- by this function is used to compare with last_id value in the input. These
-- functions are kept separate because of the cases where we want to sort by non-unique
-- values, and thus cannot be used reliably for partitioning.
buildPaginatedList
  :: ( Ord sortField
     , Eq partitionField
     , HasId b
     , IdT b ~ Id (TagT b)
     )
  => Maybe Limit
  -> Maybe partitionField
  -> (b -> partitionField)
  -> (b -> sortField)
  -> [b]
  -> PaginatedList b
buildPaginatedList mLimit mLast pFn sFn (sortOn getId -> items) = let
  paired = zip items ((\x -> (sFn x, pFn x)) <$> items)
  sorted = sortBy (\a b -> compare (fst $ snd b) (fst $ snd a) ) paired
  totalLength = length items
  limit = case mLimit of
    Just lm -> lm
    Nothing -> fromIntegral totalLength
  fromLast = case mLast of
    Just lst -> case DL.findIndex ((== lst) . snd . snd) sorted of
      Just idx -> drop (idx + 1) sorted
      Nothing -> []
    Nothing -> sorted
  final = take (fromIntegral limit) fromLast
  rest = length fromLast - length final
  lastId = coerceToPaginationId . getId . fst . last <$> NE.nonEmpty final
  paginatedData = PaginationData (fromIntegral rest) mLimit lastId
  in PaginatedList paginatedData (fst <$> final)

makeLensesFor [("plResults", "plResultsL")] ''PaginatedList

deriveJSON defaultOptions ''Limit
deriveJSON defaultOptions ''PaginationData
deriveJSON defaultOptions ''PaginatedList


instance (ToJSON a, Buildable (ForResponseLog a))
         => Buildable (ForResponseLog (PaginatedList a)) where
  build (ForResponseLog (PaginatedList pd ls)) = "{pagination: " +| buildFromJSON pd |+
    ", results: " +| buildListForResponse (take 5) (ForResponseLog ls) |+ "}"
