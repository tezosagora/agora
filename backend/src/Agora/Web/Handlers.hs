-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-|
API handlers implementation.
-}
module Agora.Web.Handlers
       ( agoraHandlers
       , metricsHandler

       -- * For tests
       , getBallots
       , getNonVoters
       , getPeriodInfo
       , getProposal
       , getProposalVotes
       , getProposals
       , getSpecificProposalVotes
       ) where

import qualified Data.Set as Set
import Data.Time.Clock (UTCTime)
import Prometheus (exportMetricsAsText)
import Servant.API.Generic (ToServant)
import Servant.Server.Generic (AsServerT, genericServerT)

import qualified Agora.Indexer.Handlers as IA
import Agora.Metrics.Capability
import Agora.Mode
import Agora.Pagination
import Agora.Types
import Agora.Web.API
import Agora.Web.Types
import qualified Agora.Web.Types as T

type AgoraHandlers m = ToServant AgoraEndpoints (AsServerT m)
type MetricsHandler m = ToServant MetricsEndpoint (AsServerT m)

-- | Server handler implementation for Agora API.
agoraHandlers :: forall m . AgoraWorkMode m => AgoraHandlers m
agoraHandlers = genericServerT AgoraEndpoints
  { aePeriod                = getPeriodInfo
  , aeProposals             = getProposals
  , aeProposal              = getProposal
  , aeSpecificProposalVotes = getSpecificProposalVotes
  , aeProposalVotes         = getProposalVotes
  , aeBallots               = getBallots
  , aeNonVoters             = getNonVoters
  }

metricsHandler :: MetricsHandler IO
metricsHandler = genericServerT MetricsEndpoint
  { meMetrics = MetricsResponse <$> exportMetricsAsText
  }

data PeriodStarts = PeriodStarts
  { psPeriodId :: PeriodId
  , psStartTime :: UTCTime
  , psPeriodType :: PeriodType
  , psPeriodSize :: Level
  , psLevelsLeft :: Level
  } deriving Show

-- | Fetch info about specific period.
-- If Nothing passed the last known period will be used.
getPeriodInfo :: forall m. AgoraWorkMode m => Maybe PeriodId -> m PeriodInfo
getPeriodInfo = IA.getPeriodInfo

-- | Get all proposals for passed period.
getProposals
  :: AgoraWorkMode m
  => PeriodId
  -> m [T.Proposal]
getProposals = IA.getProposals

-- | Get info about proposal by proposal id.
getProposal
  :: AgoraWorkMode m
  => Text
  -> Maybe PeriodId
  -> m T.Proposal
getProposal propHash mpId = IA.getProposal (encodeHash propHash) mpId

-- | Fetch proposal votes for a proposal
getSpecificProposalVotes
  :: AgoraWorkMode m
  => Text
  -> PeriodId
  -> Maybe ProposalVoteId
  -> Maybe Limit
  -> m (PaginatedList T.ProposalVote)
getSpecificProposalVotes propId periodId mLastId mLimit =
  buildPaginatedList mLimit mLastId _pvId _pvId <$> IA.getSpecificProposalVotes (encodeHash propId) periodId

-- | Fetch proposal votes for period according to pagination params.
getProposalVotes
  :: AgoraWorkMode m
  => PeriodId
  -> Maybe ProposalVoteId
  -> Maybe Limit
  -> m (PaginatedList T.ProposalVote)
getProposalVotes periodId mLastId mLimit =
  buildPaginatedList mLimit mLastId _pvId _pvId <$> IA.getProposalVotes periodId

-- | Fetch ballots for period according to pagination params.
getBallots
  :: AgoraWorkMode m
  => PeriodId
  -> Maybe BallotId
  -> Maybe Limit
  -> Maybe [Decision]
  -> m (PaginatedList T.Ballot)
getBallots periodId mLastId mLimit mDecs = do
  r <- IA.getBallots periodId
  let final = case mDecs of
        Just (Set.fromList -> dcs) -> filter (\b -> Set.member (_bDecision b) dcs) r
        Nothing -> r
  pure $ buildPaginatedList mLimit mLastId (view bId) (view bTimestamp) final

-- | Fetch nonVoters for period
getNonVoters :: AgoraWorkMode m => PeriodId -> m [T.Baker]
getNonVoters pid = sortOn (Down . _bkVotingPower) <$> IA.getNonVoters pid
