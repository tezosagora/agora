-- SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later

{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE Strict                #-}
-- | This module holds the types that represent the data exported
--   by the Tzkt indexer Api.

module Agora.Indexer.Types
  ( Ballot(..)
  , Block(..)
  , Delegate(..)
  , EpochInfo(..)
  , Head(..)
  , Operation(..)
  , PeriodInfo(..)
  , PeriodStatus(..)
  , PeriodType(..)
  , PeriodVoter(..)
  , Proposal(..)
  , ProposalOperation(..)
  , ProposalStatus(..)
  , Protocol(..)
  , SimpleProposal(..)
  , SimplePeriodInfo(..)
  , VotingPeriod(..)
  , ballotDelegate
  , ballotVotingPower
  , blockTimestamp
  , delegateAlias
  , epochIndex
  , epochPeriods
  , headLevel
  , operationDelegate
  , operationTimestamp
  , periodIndex
  , periodInfoEpoch
  , periodVoterDelegate
  , periodVoterVotingPower
  , proposalHash
  , proposalOperationDelegate
  , proposalOperationVotingPower
  , proposalOperationProposal
  , proposalVotingPower
  , simpleProposalHash
  , simplePeriodInfoEpoch
  , simplePeriodInfoPeriod
  , ballotPeriod
  ) where

import Data.Aeson
import Data.Time.Clock (UTCTime)

import qualified Agora.Types as AT
import Agora.Util

type Number = Int64

data ProposalStatus
  = Active -- the proposal in the active state
  | Accepted -- the proposal was accepted
  | Rejected -- the proposal was rejected due to too many "nay" ballots
  | Skipped -- the proposal was skipped due to the quorum was not reached
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance TagEnum ProposalStatus where
  enumDesc _ = "Proposal status"
  toTag Active   = "active"
  toTag Accepted = "accepted"
  toTag Rejected = "rejected"
  toTag Skipped  = "skipped"

instance FromJSON ProposalStatus where
  parseJSON = parseJSONTag

instance ToJSON ProposalStatus where
  toJSON = toJSONTag

data PeriodStatus
  = PActive
  | PNoProposals
  | PNoQuorum
  | PNoSupermajority
  | PSuccess
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance TagEnum PeriodStatus where
  enumDesc _ = "Period status"
  toTag PActive          = "active"
  toTag PNoProposals     = "no_proposals"
  toTag PNoQuorum        = "no_quorum"
  toTag PNoSupermajority = "no_supermajority"
  toTag PSuccess         = "success"

instance FromJSON PeriodStatus where
  parseJSON = parseJSONTag

instance ToJSON PeriodStatus where
  toJSON = toJSONTag

data PeriodType
  = Proposing     -- ^ Proposal phase (named `Proposing` to avoid name clashes with @Proposal@ datatype)
  | Exploration   -- ^ Exploration phase
  | Testing       -- ^ Testing phase
  | Promotion     -- ^ Promotion phase
  | Adoption      -- ^ Adoption phase
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance TagEnum PeriodType where
  enumDesc _ = "Period type"
  toTag Proposing   = "proposal"
  toTag Exploration = "exploration"
  toTag Testing     = "testing"
  toTag Promotion   = "promotion"
  toTag Adoption    = "adoption"

instance FromJSON PeriodType where
  parseJSON = parseJSONTag

instance ToJSON PeriodType where
  toJSON = toJSONTag

data PeriodInfo = PeriodInfo
  { kind              :: PeriodType
  , firstLevel        :: AT.Level
  , lastLevel         :: AT.Level
  , startTime         :: UTCTime
  , endTime           :: UTCTime
  , status            :: Maybe PeriodStatus
  , epoch             :: AT.EpochId
  , yayBallots        :: Maybe Number
  , yayVotingPower    :: Maybe AT.Mutez
  , nayBallots        :: Maybe Number
  , nayVotingPower    :: Maybe AT.Mutez
  , passBallots       :: Maybe Number
  , passVotingPower   :: Maybe AT.Mutez
  , ballotsQuorum     :: Maybe Float
  , supermajority     :: Maybe Float
  , totalBakers       :: Maybe Number
  , totalVotingPower  :: Maybe AT.Mutez
  , index             :: AT.PeriodId
    -- An Epoch is a sequence of voting periods that comprises of one
    -- Tezos amendment. So it will always have one period (proposal) and
    -- possible contain further periods, util the proposal is rejected or
    -- activated.
  } deriving stock (Eq, Show, Generic)

data SimplePeriodInfo = SimplePeriodInfo
  { index :: AT.PeriodId
  , epoch :: AT.EpochId
  } deriving stock (Eq, Show, Generic)

data Protocol = Protocol
  { hash       :: AT.ProtocolHash
  , firstLevel :: AT.Level
  , lastLevel  :: Maybe AT.Level
  , constants  :: AT.Constants
  } deriving (Show, Generic)

data Head = Head
  { level     :: AT.Level
  , hash      :: AT.BlockHash
  , protocol  :: AT.ProtocolHash
  , timestamp :: UTCTime
  } deriving (Eq, Show, Generic)

data Block = Block
  { level     :: AT.Level
  , timestamp :: UTCTime
  } deriving (Eq, Show, Generic)

data Delegate = Delegate
  { address :: AT.PublicKeyHash
  , alias   :: Maybe Text
  , logoUrl :: Maybe Text
  } deriving (Show, Eq, Generic)

instance Ord Delegate where
  compare d1 d2 = compare (address d1) (address d2)

data Ballot = Ballot
  { delegate      :: Delegate
  , vote          :: AT.Decision
  , votingPower   :: AT.Mutez
  , id            :: Number
  , hash          :: AT.OperationHash
  , timestamp     :: UTCTime
  , proposal      :: SimpleProposal
  , period        :: SimplePeriodInfo
  } deriving (Eq, Show, Generic)

data SimpleProposal = SimpleProposal
  { alias :: Maybe Text
  , hash  :: AT.ProposalHash
  } deriving (Show, Generic)

instance Ord SimpleProposal where
  compare a b = compare (simpleProposalHash a) (simpleProposalHash b)

instance Eq SimpleProposal where
  a == b = simpleProposalHash a == simpleProposalHash b

data EpochInfo = EpochInfo
  { index   :: AT.EpochId
  , periods :: [PeriodInfo]
  } deriving (Eq, Show, Generic)

data PeriodVoter = PeriodVoter
  { delegate    :: Delegate
  , votingPower :: AT.Mutez
  } deriving (Eq, Show, Generic)

instance Ord PeriodVoter where
  compare p1 p2 = compare (periodVoterDelegate p1) (periodVoterDelegate p2)

data Operation = Operation
  { timestamp :: UTCTime
  , level     :: AT.Level
  , hash      :: AT.OperationHash
  , proposal  :: Maybe SimpleProposal
  , delegate  :: Delegate
  } deriving (Eq, Show, Generic)

data Proposal = Proposal
  { epoch       :: AT.EpochId
  , votingPower :: AT.Mutez
  , hash        :: AT.ProposalHash
  , firstPeriod :: AT.PeriodId -- This field appear to be badly named in the indexer api.
  -- Because in the request to fetch a single proposal by hash, it returns the most recent
  -- proposal with the input hash. So the firstperiod will contain the most recent period where
  -- this proposal was injected. And not the very first period this proposal appeared. But in the
  -- response of proposal search request, which returns proposals in a list, this field contains the
  -- proper value of first period.
  , lastPeriod  :: AT.PeriodId
  , initiator   :: Delegate
  , status      :: ProposalStatus
  } deriving (Eq, Show, Generic)

data ProposalOperation = ProposalOperation
  { id          :: Number
  , hash        :: AT.OperationHash
  , proposal    :: SimpleProposal
  , timestamp   :: UTCTime
  , votingPower :: AT.Mutez
  , delegate    :: Delegate
  } deriving (Eq, Show, Generic)

data VotingPeriod = VotingPeriod
  { totalBakers    :: Maybe Number
  , proposalsCount :: Maybe Number
  , yayBallots     :: Maybe Number
  , nayBallots     :: Maybe Number
  , yayRolls       :: Maybe Number
  , nayRolls       :: Maybe Number
  , passRolls      :: Maybe Number
  , passBallots    :: Maybe Number
  , totalRolls     :: Maybe Number
  } deriving (Show, Generic)

instance FromJSON Protocol where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Protocol where
  toJSON = genericToJSON defaultOptions

instance FromJSON Operation where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Operation where
  toJSON = genericToJSON defaultOptions

instance FromJSON SimpleProposal where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON SimpleProposal where
  toJSON = genericToJSON defaultOptions

instance FromJSON EpochInfo where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON EpochInfo where
  toJSON = genericToJSON defaultOptions

instance FromJSON Block where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Block where
  toJSON = genericToJSON defaultOptions

instance FromJSON Head where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Head where
  toJSON = genericToJSON defaultOptions

instance FromJSON Ballot where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Ballot where
  toJSON = genericToJSON defaultOptions

instance FromJSON Delegate where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Delegate where
  toJSON = genericToJSON defaultOptions

instance FromJSON PeriodInfo where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON PeriodInfo where
  toJSON = genericToJSON defaultOptions

instance FromJSON PeriodVoter where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON PeriodVoter where
  toJSON = genericToJSON defaultOptions

instance FromJSON ProposalOperation where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON ProposalOperation where
  toJSON = genericToJSON defaultOptions

instance FromJSON Proposal where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Proposal where
  toJSON = genericToJSON defaultOptions

instance FromJSON VotingPeriod where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON VotingPeriod where
  toJSON = genericToJSON defaultOptions

instance FromJSON SimplePeriodInfo where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON SimplePeriodInfo where
  toJSON = genericToJSON defaultOptions

ballotDelegate :: Ballot -> Delegate
ballotDelegate = delegate

ballotVotingPower :: Ballot -> AT.Mutez
ballotVotingPower = votingPower

proposalOperationVotingPower :: ProposalOperation -> AT.Mutez
proposalOperationVotingPower = votingPower

proposalOperationDelegate :: ProposalOperation -> Delegate
proposalOperationDelegate = delegate

proposalOperationProposal :: ProposalOperation -> SimpleProposal
proposalOperationProposal = proposal

proposalHash :: Proposal -> AT.ProposalHash
proposalHash = hash

simpleProposalHash :: SimpleProposal -> AT.ProposalHash
simpleProposalHash = hash

headLevel :: Head -> AT.Level
headLevel = level

blockTimestamp :: Block -> UTCTime
blockTimestamp = timestamp

operationTimestamp :: Operation -> UTCTime
operationTimestamp = timestamp

operationDelegate :: Operation -> Delegate
operationDelegate = delegate

periodInfoEpoch :: PeriodInfo -> AT.EpochId
periodInfoEpoch = epoch

epochPeriods :: EpochInfo -> [PeriodInfo]
epochPeriods = periods

periodVoterDelegate :: PeriodVoter -> Delegate
periodVoterDelegate = delegate

periodVoterVotingPower :: PeriodVoter -> AT.Mutez
periodVoterVotingPower = votingPower

periodIndex :: PeriodInfo -> AT.PeriodId
periodIndex = index

epochIndex :: EpochInfo -> AT.EpochId
epochIndex = index

proposalVotingPower :: Proposal -> AT.Mutez
proposalVotingPower = votingPower

delegateAlias :: Delegate -> Maybe Text
delegateAlias = alias

simplePeriodInfoEpoch :: SimplePeriodInfo -> AT.EpochId
simplePeriodInfoEpoch = epoch

simplePeriodInfoPeriod :: SimplePeriodInfo -> AT.PeriodId
simplePeriodInfoPeriod = index

ballotPeriod :: Ballot -> SimplePeriodInfo
ballotPeriod = period
