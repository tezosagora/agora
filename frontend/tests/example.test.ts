// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

describe("simple test", (): void => {
  it("should add 2 + 2", (): void => {
    expect(2 + 2).toBe(4);
  });
});
