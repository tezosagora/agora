// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import { AgoraApi } from "./AgoraApi";

const axiosConfig: AxiosRequestConfig = {
  baseURL: "/api/v1",
};

const axiosIntance: AxiosInstance = axios.create(axiosConfig);

export const Api = {
  agoraApi: AgoraApi(axiosIntance),
};
