// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { Pagination } from "~/models/Pagination";
import { Proposer } from "~/models/ProposalInfo";

export interface ProposalVotesListItem {
  id: number;
  author: Proposer;
  operation: string;
  proposal: string;
  proposalTitle: string | null;
  timestamp: string;
}

export interface ProposalVotesList {
  pagination: Pagination;
  results: ProposalVotesListItem[];
}
