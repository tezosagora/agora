// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React from "react";
import { FunctionComponent } from "react";
import { Link } from "react-navi";
import { LinkProps } from "react-navi/dist/types/Link";

const NaviLink: FunctionComponent<LinkProps> = (props): JSX.Element => {
  return <Link prefetch={false} {...props}></Link>;
};
export default NaviLink;
