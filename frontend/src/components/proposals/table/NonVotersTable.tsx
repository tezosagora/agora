// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement, useState } from "react";
import cx from "classnames";
import styles from "~/styles/components/proposals/table/BakersTable.scss";
import { useTranslation } from "react-i18next";
import PointerIconSvg from "~/assets/svg/PointerIcon";
import { Proposer, formatVotingPower } from "~/models/ProposalInfo";
import NoUserIcon from "./NoUserIcon";

interface NonVotersTableItemTypes {
  item: Proposer;
}

const NonVotersTableItem: FunctionComponent<NonVotersTableItemTypes> = ({
  item,
}): ReactElement => {
  const name = (): JSX.Element | string => {
    const text = item.name ? item.name : item.pkh;
    const image = item.logoUrl ? (
      <img src={item.logoUrl} />
    ) : (
      <NoUserIcon className={styles.no_user} value={item.pkh} />
    );
    if (item.profileUrl)
      return (
        <a href={item.profileUrl}>
          {text}
          {image}
        </a>
      );
    return (
      <span>
        {text}
        {image}
      </span>
    );
  };

  return (
    <tr>
      <td className={styles.name}>{name()}</td>
      <td className={styles.vpower}>
        {formatVotingPower(item.votingPower)} {"ꜩ"}
      </td>
    </tr>
  );
};

type SortChangeHandler = (sort: { field: string; order: number }) => void;

interface NonVotersTableTypes {
  className?: string;
  data: Proposer[];
  onSortChange?: SortChangeHandler;
}

const NonVotersTable: FunctionComponent<NonVotersTableTypes> = ({
  className,
  data,
  onSortChange = (): void => {},
}): ReactElement => {
  const { t } = useTranslation();
  const initialSort = { field: "vpower", order: -1 };
  const [sort, setSort] = useState({ field: "vpower", order: -1 });

  const orderBy = (field: string): (() => void) => (): void => {
    const newSort =
      sort.order == 1
        ? initialSort
        : { order: sort.field != field ? -1 : 1, field };
    setSort(newSort);
    onSortChange(newSort);
  };

  const sortedData = (): Proposer[] => {
    if (!sort.order) return data;

    return data.sort((a, b): number => {
      let decision = 0;
      switch (sort.field) {
        case "name":
          if (a.name && !b.name) decision = -1;
          if (!a.name && b.name) decision = 1;
          if (a.name && b.name) decision = a.name.localeCompare(b.name);
          if (!a.name && !b.name) decision = a.pkh.localeCompare(b.pkh);
          break;
        case "vpower":
          decision = a.votingPower - b.votingPower;
          break;
      }
      return decision * sort.order;
    });
  };

  return (
    <table className={cx(className, styles.nonvoters)}>
      <thead>
        <tr>
          <th onClick={orderBy("name")} className={styles.name}>
            {t("proposals.bakersTable.header.baker")}
            {sort.field == "name" && (
              <PointerIconSvg
                className={sort.order == -1 ? styles.up : void 0}
              />
            )}
          </th>
          <th onClick={orderBy("vpower")} className={styles.vpower}>
            {t("proposals.bakersTable.header.votesAmount")}

            {sort.field == "vpower" && (
              <PointerIconSvg
                className={sort.order == -1 ? styles.up : void 0}
              />
            )}
          </th>
        </tr>
      </thead>
      <tbody>
        {sortedData().map(
          (item: Proposer, index: number): ReactElement => (
            <NonVotersTableItem key={index} item={item} />
          )
        )}
      </tbody>
    </table>
  );
};

export default NonVotersTable;
