// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement } from "react";
import cx from "classnames";
import DiscourseButton from "~/components/controls/DiscourseButton";
import styles from "~/styles/components/proposals/ProposalDescription.scss";
import LearnMoreButton from "../controls/LearnMoreButton";
import AlertButton from "../controls/AlertButton";

interface ExplorationInfoTypes {
  className?: string;
  title: string;
  description: string;
  discourseLink: string | null;
  learnMoreLink?: string;
}

const ProposalDescription: FunctionComponent<ExplorationInfoTypes> = ({
  title,
  description,
  discourseLink,
  learnMoreLink,
  className,
}): ReactElement => {
  return (
    <div className={cx(className, styles.explorationDescription)}>
      <h1 className={styles.explorationDescription__title}>{title}</h1>
      <div
        className={styles.explorationDescription__description}
        dangerouslySetInnerHTML={{ __html: description }}
      />
      <div className={styles.explorationDescription__buttons}>
        {learnMoreLink && (
          <LearnMoreButton
            className={styles.explorationDescription__moreButton}
            href={learnMoreLink}
          />
        )}
        {discourseLink ? (
          <DiscourseButton
            className={styles.explorationDescription__discussButton}
            href={discourseLink}
          />
        ) : null}
        <AlertButton className={styles.explorationDescription__alertButton} />
      </div>
    </div>
  );
};

export default ProposalDescription;
