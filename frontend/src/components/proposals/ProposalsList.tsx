// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement, useState } from "react";
import cx from "classnames";
import Card from "~/components/common/Card";
import * as styles from "~/styles/components/proposals/ProposalsList.scss";
import {
  ProposalsList as ProposalsListType,
  ProposalsListItem,
} from "~/models/ProposalsList";
import { ProposalPeriodInfo } from "~/models/Period";
import ChatIcon from "~/assets/svg/ChatIcon";
import InfoIcon from "~/assets/svg/InfoIcon";
import { useTranslation } from "react-i18next";
import { useNavigation } from "react-navi";
import { formatVotingPower } from "~/models/ProposalInfo";
import NaviLink from "../common/NaviLink";

interface ProposalsListItemTypes {
  proposal: ProposalsListItem;
  period: ProposalPeriodInfo;
  votesAvailable: number;
}

const ProposalListItem: FunctionComponent<ProposalsListItemTypes> = ({
  proposal,
  period,
  votesAvailable,
}): ReactElement => {
  const discourseLink = proposal.discourseLink ? proposal.discourseLink : null;
  const { t } = useTranslation();
  const [changedVotes, changeVotes] = useState(false);
  const [changedProposal, changeProposal] = useState(false);

  const navigation = useNavigation();

  const handleChangeVotes = (e: React.MouseEvent): void => {
    e.stopPropagation();
    changeVotes((_prev): boolean => !_prev);
  };

  let isResubmission = proposal.firstPeriod < period.period.id;

  return (
    <Card className={styles.list__item}>
      <div className={styles.list__item__info}>
        <div
          onClick={(): void => {
            navigation.navigate(
              `/proposal/${proposal.hash}/${period.period.id}#voters`
            );
          }}
          className={styles.list__item__upvotes}
        >
          <a className={styles.list__item__upvotes__title}>
            {t("proposals.proposalsList.upvotesCaption")}
          </a>
          <div
            className={styles.list__item__upvotes__value}
            onClick={handleChangeVotes}
          >
            {t(`proposals.proposalsList.upvotesValue`, {
              percent: changedVotes ? "%" : "",
              value: changedVotes
                ? ((proposal.votesCasted / votesAvailable) * 100).toFixed(2)
                : formatVotingPower(proposal.votesCasted),
            })}
          </div>
        </div>
        <div className={styles.list__item__main}>
          <div className={styles.list__item__name}>
            {proposal.proposer.name
              ? proposal.proposer.name
              : proposal.proposer.pkh}
            <span className={styles.list__item__resubmission}>
              {isResubmission
                ? `(Re-submission from period ${proposal.firstPeriod})`
                : ""}
            </span>
          </div>
          <div
            className={styles.list__item__title}
            onClick={(): void => changeProposal(!changedProposal)}
          >
            {!proposal.title || changedProposal
              ? proposal.hash
              : proposal.title}
          </div>
          <div
            className={styles.list__item__description}
            dangerouslySetInnerHTML={{
              __html: proposal.shortDescription
                ? proposal.shortDescription
                : t("proposals.proposalsList.noDescriptionCaption"),
            }}
          />
          <div className={styles.list__item__buttons}>
            <NaviLink href={`/proposal/${proposal.hash}/${period.period.id}`}>
              <InfoIcon />
              {t("proposals.proposalsList.learnMore")}
            </NaviLink>
            {discourseLink ? (
              <a href={discourseLink}>
                <ChatIcon />
                {t("proposals.proposalsList.discuss")}
              </a>
            ) : null}
          </div>
        </div>
      </div>
    </Card>
  );
};

interface ProposalsListTypes {
  className?: string;
  proposals: ProposalsListType;
  votesAvailable: number;
  period: ProposalPeriodInfo;
}

const ProposalsList: FunctionComponent<ProposalsListTypes> = ({
  className,
  proposals,
  votesAvailable,
  period,
}): ReactElement => {
  return (
    <div className={cx(className, styles.list)}>
      {proposals.map(
        (proposal, index): ReactElement => (
          <ProposalListItem
            proposal={proposal}
            period={period}
            votesAvailable={votesAvailable}
            key={index}
          />
        )
      )}
    </div>
  );
};

export default ProposalsList;
