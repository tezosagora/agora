// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group, 2021-2022 Tezos Commons
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement } from "react";
import cx from "classnames";
import ReactResizeDetector from "react-resize-detector";
import { BallotsStats, VoteStats } from "~/models/Period";
import { calculateBarsArray } from "~/utils/majorityGraphUtils";
import styles from "~/styles/components/proposals/graphs/MajorityGraph.scss";
import { useTranslation } from "react-i18next";

type MajorityGraphType = "quorum" | "majority";

interface GraphTypes {
  width: number;
  ballotsStats: BallotsStats;
  voteStats: VoteStats;
  type: MajorityGraphType;
  markCaption: string;
  markTooltip: string;
  markValue: number;
}

const Graph: FunctionComponent<GraphTypes> = (props): ReactElement => {
  const barArray = calculateBarsArray(
    props.width,
    props.ballotsStats,
    props.voteStats
  );
  let children: ReactElement[] = [];
  let count = 0;
  barArray.forEach((b): void => {
    children.push(
      <div
        className={styles.graphBar}
        key={count}
        style={{
          left: `calc(100% * ${b.startX / props.width})`,
          width: `calc(100% * ${(b.endX - b.startX) / props.width})`,
          backgroundColor: b.color,
        }}
      ></div>
    );
    count = count + 1;
  });
  return (
    <div style={{ position: "relative" }}>
      <div className={styles.caption} title={props.markTooltip}>
        {props.markCaption}
      </div>
      <div
        style={{ left: `${props.markValue}%` }}
        className={styles.chevron}
      ></div>
      <div className={styles.graphContainer}>{children}</div>
    </div>
  );
};

interface MajorityGraphTypes {
  className?: string;
  ballotsStats: BallotsStats;
  voteStats: VoteStats;
}

const MajorityGraph: FunctionComponent<MajorityGraphTypes> = ({
  className,
  ballotsStats,
  voteStats,
}): ReactElement => {
  const majorityBallots: BallotsStats = {
    ...ballotsStats,
    pass: 0,
  };
  const majorityVotes: VoteStats = {
    votesAvailable: majorityBallots.yay + majorityBallots.nay,
    votesCast: majorityBallots.yay + majorityBallots.nay,
    numVoters: voteStats.numVoters,
    numVotersTotal: voteStats.numVotersTotal,
  };

  const { t } = useTranslation();

  return (
    <div className={cx(className, styles.majorityGraph)}>
      <ReactResizeDetector
        handleWidth
        render={({ width = 0 }): ReactElement => (
          <>
            <Graph
              width={width}
              ballotsStats={majorityBallots}
              voteStats={majorityVotes}
              type="majority"
              markTooltip={t("proposals.majorityGraph.supermajorityTooltip")}
              markCaption={t("proposals.majorityGraph.supermajorityCaption", {
                target: ballotsStats.supermajority,
                value: (
                  (ballotsStats.yay / (ballotsStats.nay + ballotsStats.yay)) *
                  100
                ).toFixed(2),
              })}
              markValue={ballotsStats.supermajority}
            />
            <Graph
              width={width}
              ballotsStats={ballotsStats}
              voteStats={voteStats}
              type="quorum"
              markTooltip={t("proposals.majorityGraph.quorumTooltip")}
              markCaption={t("proposals.majorityGraph.quorumCaption", {
                target: ballotsStats.quorum,
                value: (
                  (voteStats.votesCast / voteStats.votesAvailable) *
                  100
                ).toFixed(2),
              })}
              markValue={ballotsStats.quorum}
            />
          </>
        )}
      />
    </div>
  );
};

export default MajorityGraph;
