// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement, useContext } from "react";
import styles from "~/styles/components/controls/ThemeSwitcher.scss";
import ThemeSwitcherIcon from "~/assets/svg/ThemeSwitcherIcon";
import { ThemeContext } from "~/components/common/ThemeContext";

interface ThemeSwitcherTypes {
  toggleTheme: () => void;
}

const ThemeSwitcher: FunctionComponent<ThemeSwitcherTypes> = ({
  toggleTheme,
}): ReactElement => {
  const themeContext = useContext(ThemeContext);
  return (
    <div onClick={toggleTheme} className={styles.container}>
      <ThemeSwitcherIcon mode={themeContext.currentTheme} />
      <div className={styles.slider_container}>
        <div className={styles.slider}></div>
      </div>
    </div>
  );
};

export default ThemeSwitcher;
