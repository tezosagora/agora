// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { ReactElement } from "react";
import WelcomePage from "~/pages/WelcomePage";
import PeriodPage from "~/pages/proposals/PeriodPage";
import ProposalInfoPage from "~/pages/proposals/ProposalInfoPage";
import { mount, route, Matcher } from "navi";
import PeriodStore from "~/store/actions/periodActions";
import ProposalStore from "~/store/actions/proposalActions";
import { useDispatch } from "react-redux";
import LearnPage from "~/pages/LearnPage";

export default function agoraRouter(): Matcher<object, object> {
  const dispatch = useDispatch();
  return mount({
    "/": route({
      getView: async (): Promise<ReactElement> => {
        await dispatch(await PeriodStore.actionCreators.fetchWelcomePage());
        return <WelcomePage />;
      },
    }),
    "/period": route({
      getView: async (): Promise<ReactElement> => {
        await dispatch(await PeriodStore.actionCreators.fetchPeriod());
        return <PeriodPage />;
      },
    }),
    "/learn": route({
      getView: async (): Promise<ReactElement> => {
        await dispatch(await PeriodStore.actionCreators.fetchPeriod());
        return <LearnPage source={require("../assets/learning-page.md")} />;
      },
    }),
    "/period/:id": route(
      async (request): Promise<object> => {
        await dispatch(
          await PeriodStore.actionCreators.fetchPeriod(
            parseInt(request.params.id)
          )
        );
        return {
          view: <PeriodPage />,
        };
      }
    ),
    "/proposal/:id/:period_id": route(
      async (request): Promise<object> => {
        await dispatch(
          await ProposalStore.actionCreators.fetchProposal(
            request.params.id,
            request.params.period_id
          )
        );
        return {
          view: <ProposalInfoPage />,
        };
      }
    ),
  });
}
