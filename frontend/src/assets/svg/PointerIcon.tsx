// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement } from "react";
import styles from "~/styles/components/common/Svg.scss";
import cx from "classnames";

interface PointerIconProps {
  className?: string;
}

const PointerIconSvg: FunctionComponent<PointerIconProps> = ({
  className,
}): ReactElement => (
  <svg width={8} height={9} className={cx(className, styles.invertible_svg)}>
    <g fillRule="evenodd" strokeWidth="1.4" opacity=".5">
      <path d="M1 5l3 3 3-3M4 8V0" />
    </g>
  </svg>
);

export default PointerIconSvg;
