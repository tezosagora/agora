// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { ReactElement } from "react";
import styles from "~/styles/components/common/Svg.scss";

const MenuIcon = (): ReactElement => (
  <svg width={24} height={20} className={styles.invertible_svg}>
    <path
      d="M0 2h24M0 10h24M0 18h24"
      fill="none"
      fillRule="evenodd"
      stroke="#2C7DF7"
      strokeLinejoin="round"
      strokeWidth={3}
    />
  </svg>
);

export default MenuIcon;
