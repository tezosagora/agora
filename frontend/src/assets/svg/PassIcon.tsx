// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { ReactElement } from "react";

const SvgPassIcon = (): ReactElement => (
  <svg width={44} height={44} fill="none">
    <circle cx={22} cy={22} r={22} fill="#2C7DF7" fillOpacity={0.2} />
    <path fill="#2C7DF7" d="M14 19h16v6H14z" />
  </svg>
);

export default SvgPassIcon;
