// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement } from "react";
import styles from "~/styles/components/common/Svg.scss";
import cx from "classnames";

interface Props {
  className?: string;
}

const AngleIcon: FunctionComponent<Props> = ({ className }): ReactElement => (
  <svg width={11} height={18} className={cx(className, styles.invertible_svg)}>
    <path
      fill="none"
      stroke="#123262"
      strokeWidth={2}
      d="M1 1l8 8-8 8"
      opacity={0.4}
    />
  </svg>
);

export default AngleIcon;
