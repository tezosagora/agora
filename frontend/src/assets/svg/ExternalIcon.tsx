// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { ReactElement } from "react";
import styles from "~/styles/components/common/Svg.scss";

const ExternalIcon = (): ReactElement => (
  <svg width={12} height={12} className={styles.invertible_svg}>
    <path
      fill="#FFF"
      d="M8.586 2H5V0h7v7h-2V3.414L4.707 8.707 3.293 7.293 8.586 2zM11 10v2H0V1h2v9h9z"
    />
  </svg>
);

export default ExternalIcon;
