// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { ReactElement } from "react";
import styles from "~/styles/components/common/Svg.scss";

const ChatIcon = (): ReactElement => (
  <svg width={12} height={12} className={styles.invertible_svg}>
    <g fill="none" fillRule="evenodd" stroke="#2C7DF7">
      <path d="M.5 6.793L1.793 5.5H8a.5.5 0 00.5-.5V1A.5.5 0 008 .5H1a.5.5 0 00-.5.5v5.793z" />
      <path d="M10.103 4.5L3.5 7.33V9a.5.5 0 00.5.5h6.207l1.293 1.293V5a.5.5 0 00-.5-.5h-.897z" />
    </g>
  </svg>
);

export default ChatIcon;
