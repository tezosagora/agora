// SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React, { FunctionComponent, ReactElement } from "react";
import styles from "~/styles/components/common/Svg.scss";

const CloseIcon: FunctionComponent = (): ReactElement => (
  <svg width={27} height={26} className={styles.invertible_svg}>
    <path
      d="M2 1l24 24M2 25L26 1"
      fill="none"
      fillRule="evenodd"
      stroke="#2C7DF7"
      strokeLinejoin="round"
      strokeWidth={3}
    />
  </svg>
);

export default CloseIcon;
