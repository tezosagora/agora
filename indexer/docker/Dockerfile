# SPDX-FileCopyrightText: 2023 Tezos Commons
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from ubuntu:22.04
ENV DEBIAN_FRONTEND=noninteractive
RUN adduser --disabled-password --disabled-login tzidx
RUN usermod -aG sudo tzidx
ENV LANG=en_US.UTF-8
RUN chmod 777 /tmp
RUN apt-get update
RUN apt-get install -y locales && \
    sed -i -e "s/# $LANG.*/$LANG UTF-8/" /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=$LANG
RUN bash -c 'apt-get install -y lsb-release gnupg wget zlib1g-dev libgmp-dev'
RUN \
    apt-get update -y && \
    apt-get install -y --no-install-recommends \
        curl \
        libnuma-dev \
        zlib1g-dev \
        libgmp-dev \
        libgmp10 \
        git \
        wget \
        lsb-release \
        software-properties-common \
        gnupg2 \
        apt-transport-https \
        gcc \
        autoconf \
        automake \
        build-essential
RUN curl https://downloads.haskell.org/~ghcup/x86_64-linux-ghcup > /usr/bin/ghcup && chmod +x /usr/bin/ghcup
RUN bash -c "ghcup upgrade"
RUN bash -c "ghcup install cabal 3.6.2.0"
RUN bash -c "ghcup set cabal 3.6.2.0"


# Add ghcup to PATH
ENV PATH=${PATH}:/root/.local/bin
ENV PATH=${PATH}:/root/.ghcup/bin
ENV PATH=${PATH}:/root/.cabal/bin

RUN bash -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
RUN bash -c 'wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -'
RUN apt-get update
RUN bash -c "apt-get -y install 'postgresql-14' libpq-dev"
RUN bash -c "apt-get install -y hpack"

# Install GHC
RUN bash -c "ghcup install ghc 9.2.5"
RUN bash -c "ghcup set ghc 9.2.5"
#
RUN bash -c "cabal update"
ARG repobranch=master
RUN bash -c "git clone https://gitlab.com/tezosagora/agora.git -b $repobranch"
WORKDIR /agora/indexer

RUN bash -c "hpack"
RUN bash -c "cabal build && cabal install"
RUN bash -c 'apt-get install -y sudo'

ENV PATH=${PATH}:/usr/lib/postgresql/14/bin
COPY startup.sh startup.sh
RUN bash -c "chmod +x startup.sh"

USER root
CMD ["./startup.sh"]
