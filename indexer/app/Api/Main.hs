-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Main where

import Server

-- | The entrypoint of the API component. This is separated from the
-- sync component since they are logically separate concerns. Having
-- this separation also allows to check logs and monitor resources for
-- them separately.
main :: IO ()
main = serveApi
