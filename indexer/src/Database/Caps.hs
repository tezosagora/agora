-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | Definition for the Database capability.
module Database.Caps where

import Control.Monad.IO.Class
import Database.PostgreSQL.Simple
import Logging
import Monad.Capabilities

-- | The database interface capability.
data DbCap m = DbCap
  { _withConnection :: forall a. (Connection -> m a) -> m a
  }

$(makeCap ''DbCap)

-- | Accepting a database connection implement the database capability. We
-- don't use connection pooling because the indexer runs in a single thread.
-- The api can potentially trigger access from multiple threads, but since the
-- consumer of api is another local process that runs in a single thread (agora
-- backend) it should not make much of a difference.
--
-- Using a connection pool will also make using db transactions a bit less
-- straight forward, since transactions are scoped to a connection. And if the
-- calling code fetches a connection multiple times during the processing of a
-- block, then we should make sure that the code is always provided the same
-- connection.
--
-- So if we ever need to use pooling, it can be restricted to the API component
-- and not the indexer component. This because api is read only, so does not
-- need to use database transactions.
dbCap :: MonadIO m => Connection -> CapImpl DbCap '[LoggingCap] m
dbCap conn = CapImpl $ DbCap { _withConnection = \cb -> cb conn }
