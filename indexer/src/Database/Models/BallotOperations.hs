-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database.Models.BallotOperations
  ( Ballot
  , BallotDbRead
  , BallotPoly(..)
  , BallotId
  , ballotsTable
  ) where

import Database.Mapping
import Database.Models.Proposals as Proposals
import Database.Models.PeriodVoters as PeriodVoters
import Node qualified
import System.Log.FastLogger

type BallotId = DbId "ballot"

data BallotPoly id proposalId periodIdx operationHash periodVoterId ballot sourceBlockId = Ballot
  { id            :: id
  , proposalId    :: proposalId
  , periodIdx     :: periodIdx
  , operationHash :: operationHash
  , periodVoterId :: periodVoterId
  , ballot        :: ballot
  , sourceBlockId :: sourceBlockId
  } deriving stock Show

instance
  ( Show id, Show proposalId, Show periodIdx
  , Show operationHash, Show voter, Show ballot
  , Show sourceBlockId) =>
  ToLogStr
    (BallotPoly id proposalId periodIdx operationHash
      voter ballot sourceBlockId) where
    toLogStr = toLogStr . show

$(makeAdaptorAndInstance "pDbBallot" ''BallotPoly)

type Ballot'' rm = BallotPoly
  (ComputeFieldTypeAuto rm BallotId)
  (ComputeFieldType rm Proposals.ProposalId)
  (ComputeFieldType rm Node.PeriodIdx)
  (ComputeFieldType rm Node.OperationHash)
  (ComputeFieldType rm PeriodVoters.PeriodVoterId)
  (ComputeFieldType rm Node.Ballot)
  (ComputeFieldType rm Int)

type Ballot = Ballot'' 'HaskellRead
type BallotDbRead = Ballot'' 'DbRead

ballotsTable
  :: Table (Ballot'' 'DbWrite) (Ballot'' 'DbRead)
ballotsTable =
  table "ballot_operations" (pDbBallot
    (Ballot
      (tableField "id")
      (tableField "proposal_id")
      (tableField "period_idx")
      (tableField "operation_hash")
      (tableField "period_voter_id")
      (tableField "ballot")
      (tableField "source_block_id")))
