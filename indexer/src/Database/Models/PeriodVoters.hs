-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database.Models.PeriodVoters
  ( PeriodVoter
  , PeriodVoterPoly(..)
  , PeriodVoterId
  , periodVotersTable
  ) where

import Database.Mapping
import Database.Models.Blocks qualified as Blocks
import Database.Models.Voters qualified as Voters
import Node qualified

type PeriodVoterId = DbId "periodVoter"

data PeriodVoterPoly id voterId periodIdx votingPower blockId = PeriodVoter
  { id            :: id
  , voterId         :: voterId
  , periodIdx     :: periodIdx
  , votingPower   :: votingPower
  , sourceBlockId :: blockId
  }

$(makeAdaptorAndInstance "pDbPeriodVoter" ''PeriodVoterPoly)

type PeriodVoter'' rm = PeriodVoterPoly
  (ComputeFieldTypeAuto rm PeriodVoterId)
  (ComputeFieldType rm Voters.VoterId)
  (ComputeFieldType rm Node.PeriodIdx)
  (ComputeFieldType rm Node.VotingPower)
  (ComputeFieldType rm Blocks.BlockId)

type PeriodVoter = PeriodVoter'' 'HaskellRead

periodVotersTable
  :: Table (PeriodVoter'' 'DbWrite) (PeriodVoter'' 'DbRead)
periodVotersTable =
  table "period_voters" (pDbPeriodVoter
    (PeriodVoter
      (tableField "id")
      (tableField "voter_id")
      (tableField "period_idx")
      (tableField "voting_power")
      (tableField "source_block_id")))
