-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Database.Models.Voters
  ( Voter
  , VoterPoly(..)
  , VoterId
  , votersTable
  ) where

import Database.Mapping
import Database.Models.Blocks qualified as Blocks
import Node qualified

type VoterId = DbId "voter"

data VoterPoly id voter blockId = Voter
  { id            :: id
  , voterAddress  :: voter
  , sourceBlockId :: blockId
  }

$(makeAdaptorAndInstance "pDbVoter" ''VoterPoly)

type Voter'' rm = VoterPoly
  (ComputeFieldTypeAuto rm VoterId)
  (ComputeFieldType rm Node.PkHash)
  (ComputeFieldType rm Blocks.BlockId)

type Voter = Voter'' 'HaskellRead

votersTable
  :: Table (Voter'' 'DbWrite) (Voter'' 'DbRead)
votersTable =
  table "voters" (pDbVoter
    (Voter
      (tableField "id")
      (tableField "voter_address")
      (tableField "source_block_id")))
