-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | The module that implements pooling of http servers and provides a
-- capability that can execute requests after selecting a suitable server from
-- the pool. Failures except for 404 errors, are retried, possibily using a
-- different server from the pool. 404 errors are thrown to let the
-- caller decide retry logic for the same.
module ServerPool
  ( NotFoundError(..)
  , ServerInfo (..)
  , ServerInfos
  , ServerPoolCap
  , ServerPoolRefs (..)
  , ServerStatus(..)
  , ServerConfig (..)
  , mkServerInfos
  , runRequest
  , serverPoolCap
  , emptyServerConfig
  , waitFor
  ) where

import Control.Concurrent (threadDelay)
import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TVar
import Control.Monad.Catch hiding (handle)
import Control.Monad.IO.Class
import qualified Data.List as DL
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.Text as T
import Data.Time.Clock (NominalDiffTime)
import Data.Time.Clock.POSIX
import Data.Typeable
import Logging
import Monad.Capabilities
import Network.HTTP.Client
import Network.HTTP.Client.TLS
import Network.HTTP.Types.Status
import Servant.Client
import System.Log.FastLogger
import System.Timeout

import Common

data NotFoundError
  = NotFoundError T.Text
  deriving stock Show

instance Exception NotFoundError

requestTimeout :: Int
requestTimeout = 30000000 -- 30 Seconds

data ServerConfig = ServerConfig
  { minSecsBetweenReqs :: NominalDiffTime
  , maxConcurrentRequests :: Int
  } deriving Show

defaultMinimumTimeBetweenRequests :: NominalDiffTime
defaultMinimumTimeBetweenRequests = 1

emptyServerConfig :: ServerConfig
emptyServerConfig = ServerConfig
  { minSecsBetweenReqs = defaultMinimumTimeBetweenRequests
  , maxConcurrentRequests = 5
  }

data ServerStatus = ServerStatus
  { lastRequestAt :: Maybe POSIXTime
  , active :: Bool
  , ongoingRequests :: Int
  , requestsCount :: Integer
  , successCount :: Integer
  , averageResponseTime :: Maybe NominalDiffTime
  , timeoutsCount :: Integer
  }

instance Show ServerStatus where
  show ss =
    "Last Request: " <> (show ss.lastRequestAt)
      <> ", ongoing request count: " <> (show ss.ongoingRequests)

instance ToLogStr (Map.Map BaseUrl ServerStatus) where
  toLogStr ms = toLogStr $ T.intercalate "\n" $ toTxt <$> Map.assocs ms
    where
      toTxt :: (BaseUrl, ServerStatus) -> T.Text
      toTxt (b, ss) =
          T.concat [T.pack $ showBaseUrl b, ":",  T.pack $ show ss]

newtype RequestId = RequestId Integer
  deriving newtype (Num, Eq, Ord)

instance Show RequestId where
  show (RequestId i) = "RequestId: " <> show i

data ServerPoolCap m = ServerPoolCap
  { _runRequest :: forall a. Typeable a => ClientM a -> m a
  }

$(makeCap ''ServerPoolCap)

data ServerInfo = ServerInfo
  { sendReq :: forall a. Typeable a => ClientM a -> IO (Either ClientError a)
  , config :: ServerConfig
  , serverStatus :: ServerStatus
  , baseUrl :: BaseUrl
  }

instance Show ServerInfo where
  show si = show ("baseUrl="::String, showBaseUrl si.baseUrl, "config="::String, si.config, "serverStatus="::String, si.serverStatus)

-- A type just to use for the required sort
-- ordering of the servers based on the last time
-- one was used.
data TimeSinceLastRequest
  = HaventUsedIn NominalDiffTime
  | NeverUsed
  deriving (Eq, Ord, Show)

withServer
  :: forall m caps a. (Typeable a, MonadThrow m, MonadIO m, HasCaps '[ServerPoolCap, LoggingCap] caps)
  => TVar ServerInfos
  -> (RequestId, ClientM a, Set.Set BaseUrl, Maybe POSIXTime) -- The third argument is the set of servers that have returned a 404 for this request.
  -> CapsT caps m a
withServer spr req@(reqId, action, tried, mReqInitTime) = do
  ct <- liftIO getPOSIXTime
  case mReqInitTime of
      Nothing -> do
        logDebug "Start processing request..."
        withServer spr (reqId, action, tried, Just ct)
      Just rInitTime -> do
        -- If we have been retrying for too long, forget previous retry attempts.
        let timeSinceInit = ct - rInitTime
        if timeSinceInit > abortRequestTimeout
          then do
            serverInfos <- liftIO $ readTVarIO spr
            logDebug $ toLogStr
              $ "================================="
              <> showt reqId
              <> ": Retry timout reached, forgetting previous attempts. Current status is: Tried servers: "
              <> showt tried
              <> "Server statuses:"
              <> showt (Map.elems serverInfos)
              <> "================================="
            withServer spr (reqId, action, Set.empty, Nothing)
          else do
            logDebug $ toLogStr $ showt reqId <> ": Time since init:" <> showt timeSinceInit
            (mServer, serverInfos) <- liftIO $ atomically $ do
              serverInfos <- readTVar spr
              let
                servers = Map.elems serverInfos
                eligableServers =
                  DL.filter
                    (\ss -> filterMinTimeBtwReq ct ss && filterMaxConcurrentRequests ss && not (Set.member ss.baseUrl tried))
                    servers
              case eligableServers of
                [] -> pure (Nothing, serverInfos)
                _ -> let
                  cmpFn s1 s2 = secsSinceLastUse ct s1 `compare` secsSinceLastUse ct s2
                  in pure (Just (DL.maximumBy cmpFn eligableServers, DL.length servers), serverInfos)
            case mServer of
              Nothing -> do
                  logDebug $ toLogStr $ showt reqId
                    <> ": No eligable servers at:" <> showt ct
                    <> showt (Map.elems serverInfos)
                    <> ":Tried servers: "
                    <> showt tried
                    <> "\nWaiting a bit..."
                  waitFor defaultMinimumTimeBetweenRequests
                    -- Here we wait `defaultMinimumTimeBetweenRequests`, but we should
                    -- ideally only wait for minimun time amoung all the
                    -- @minSecsBetweenReqs@ value for all the available servers. Since
                    -- this is always assigned this default value, and is not
                    -- configurable by the user, it should be good for now.
                  withServer spr req
              Just (ss, serverCount) -> do
                logDebug $ toLogStr $ showt reqId <> ": Using server " <> T.pack (showBaseUrl ss.baseUrl)
                (reqResult, reqTime) <- do
                  liftIO $ bracket_
                    (atomically $ modifyTVar' spr (updateServerStateBeforeReq ct ss.baseUrl))
                    (atomically $ modifyTVar' spr (decOngoingRequests ss.baseUrl))
                    (timeWithTimeout requestTimeout (sendReq ss action))

                case reqResult of
                  Just (Right x) -> do
                    logDebug $ toLogStr $ showt reqId <> ": Request success from " <> T.pack (showBaseUrl ss.baseUrl)
                    afterReqAction ss.baseUrl (Right reqTime)
                    pure x
                  Just (Left x) -> do
                    logDebug $ toLogStr $ showt reqId <> ": Error response, " <> showt x
                    afterReqAction ss.baseUrl (Left (Just x))
                    let
                      retryAction newTried = do
                        logDebug $ toLogStr $ showt reqId <> ": Retrying..."
                        logDebug $ toLogStr $ showt reqId <> ": tried servers:" <> showt newTried
                        withServer spr (reqId, action, newTried, mReqInitTime)
                    case x of
                      FailureResponse _ r ->
                        -- Only throw the error if the response if 404, for everything else,
                        -- retry. This is because retrying logic of 404 not found errors are
                        -- better delegated to calling code in this context. But we don't
                        -- throw 404 till we have tried through all the available server.
                        -- This is in part because non-archive nodes throw 404 if equested
                        -- info that they have dropped. So we might abort a request if it was
                        -- initially sent to a non-archive node, without even trying the
                        -- archive node that is on the server list.
                        if r.responseStatusCode == status404
                           then do
                              let newTried = Set.insert ss.baseUrl tried
                              if Set.size newTried == serverCount
                                then throwM (NotFoundError $ showt x)
                                else retryAction newTried
                            else retryAction tried
                      _ -> retryAction tried
                  Nothing -> do
                    afterReqAction ss.baseUrl (Left Nothing)
                    logDebug $ toLogStr $ showt reqId <> ": Timed out, retrying..."
                    withServer spr req
  where
    timeWithTimeout
      :: forall b. Int
      -> IO b
      -> IO (Maybe b, NominalDiffTime)
    timeWithTimeout timeoutDuration act = do
      starttime <- getPOSIXTime
      r <- timeout timeoutDuration act
      endtime <- getPOSIXTime
      pure (r, endtime - starttime)

    afterReqAction
      :: MonadIO m1
      => BaseUrl
      -> Either (Maybe ClientError) NominalDiffTime
      -> m1 ()
    afterReqAction baseUrl reqRes = liftIO $ atomically $
      modifyTVar' spr (updateServerStateAfterReq reqRes baseUrl)

    decOngoingRequests
      :: BaseUrl
      -> ServerInfos
      -> ServerInfos
    decOngoingRequests baseUrl serverInfos = let
      updateFn si = Just $ si
          { serverStatus = si.serverStatus
            { ongoingRequests = si.serverStatus.ongoingRequests - 1
            }
          }
      in Map.update updateFn baseUrl serverInfos

    updateServerStateAfterReq
      :: Either (Maybe ClientError) NominalDiffTime
      -> BaseUrl
      -> ServerInfos
      -> ServerInfos
    updateServerStateAfterReq reqRes baseUrl serverInfos = case reqRes of
      Right responseTime ->
        Map.update (updateOnSuccess responseTime) baseUrl serverInfos
      Left (Just err) -> Map.update (updateOnError err) baseUrl serverInfos
      Left Nothing -> Map.update updateOnTimeout baseUrl serverInfos

    -- On timeout, update total request count and timeoutcount
    updateOnTimeout :: ServerInfo -> Maybe ServerInfo
    updateOnTimeout si = Just $ si
        { serverStatus = si.serverStatus
          { requestsCount = si.serverStatus.requestsCount + 1
          , timeoutsCount = si.serverStatus.timeoutsCount +1
          }
        }

    -- On failure response, update total request count only
    updateOnError :: ClientError -> ServerInfo -> Maybe ServerInfo
    updateOnError _ si = Just $ si
      { serverStatus = si.serverStatus
        { requestsCount = si.serverStatus.requestsCount + 1
        }
      }

    -- On success response, update total request count, success count
    -- and average response time
    updateOnSuccess :: NominalDiffTime -> ServerInfo -> Maybe ServerInfo
    updateOnSuccess responseTime si = let
      newSuccessCount = si.serverStatus.successCount + 1
      reqCount = si.serverStatus.requestsCount + 1
      art = case si.serverStatus.averageResponseTime of
        Just currentAvg ->
          -- Compute the new average response time
          (currentAvg *
            (realToFrac si.serverStatus.successCount) + responseTime) / realToFrac newSuccessCount
        Nothing -> responseTime
      in Just $ si
          { serverStatus = si.serverStatus
            { requestsCount = reqCount
            , successCount = newSuccessCount
            , averageResponseTime = Just art
            }
          }

    updateServerStateBeforeReq
      :: POSIXTime
      -> BaseUrl
      -> ServerInfos
      -> ServerInfos
    updateServerStateBeforeReq reqTime baseUrl serverInfos = let
      updateFn
        :: ServerInfo -> Maybe ServerInfo
      updateFn si =
        Just $ si
          { serverStatus = si.serverStatus
              { ongoingRequests = si.serverStatus.ongoingRequests + 1
              , lastRequestAt = Just reqTime
              }
          }
      in Map.update updateFn baseUrl serverInfos

    secsSinceLastUse ct si = case si.serverStatus.lastRequestAt of
      Just lrt -> HaventUsedIn $ ct - lrt
      Nothing -> NeverUsed
    filterMinTimeBtwReq ct si = case si.serverStatus.lastRequestAt of
      Just lrt -> si.config.minSecsBetweenReqs <= (ct - lrt)
      Nothing -> True
    filterMaxConcurrentRequests ss =
      ss.config.maxConcurrentRequests >= ss.serverStatus.ongoingRequests

type ServerInfos = Map.Map BaseUrl ServerInfo

data ServerPoolRefs = ServerPoolRefs
  { serverInfoRef :: TVar ServerInfos
  , requestIdCounterRef :: TVar RequestId
  }

mkServerInfos
  :: [BaseUrl]
  -> IO ServerPoolRefs
mkServerInfos urls = do
  reqIdRef <- newTVarIO 10000
  serverInfos <- Map.fromList <$> mapM mkServerInfo urls >>= newTVarIO
  pure $ ServerPoolRefs serverInfos reqIdRef
  where
    mkServerInfo :: BaseUrl -> IO (BaseUrl, ServerInfo)
    mkServerInfo url = do
        manager <- newTlsManagerWith $ tlsManagerSettings
          { managerResponseTimeout = responseTimeoutMicro (10 * 1000000)
          }
        let ce = mkClientEnv manager url
        pure
          (url
          , ServerInfo (`runClientM` ce) emptyServerConfig
              (ServerStatus Nothing True 0 0 0 Nothing 0) url
          )


abortRequestTimeout :: POSIXTime
abortRequestTimeout = 60 -- 1 min

serverPoolCap
  :: (MonadThrow m, MonadIO m)
  => ServerPoolRefs
  -> CapImpl ServerPoolCap '[ServerPoolCap, LoggingCap] m
serverPoolCap spr =
  CapImpl $
    ServerPoolCap
      { _runRequest = \act -> do
          requestId <- liftIO $ atomically $ stateTVar spr.requestIdCounterRef $
              \v -> let nextVal = v + 1 in (nextVal, nextVal)
          logDebug $ toLogStr $ showt requestId <> ": Initializing... "
          withServer spr.serverInfoRef (requestId, act, Set.empty, Nothing)
      }

waitFor
  :: MonadIO m
  => NominalDiffTime
  -> m ()
waitFor secs = liftIO $ threadDelay (round $ 1000000 * secs)
