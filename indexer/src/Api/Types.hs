-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | The types used in API requests/responses.
module Api.Types
  ( Ballot(..)
  , Block(..)
  , Constants(..)
  , Delegate(..)
  , EpochInfo(..)
  , Head(..)
  , Number
  , PeriodInfo(..)
  , PeriodStatus(..)
  , PeriodType(..)
  , PeriodVoter(..)
  , Proposal(..)
  , ProposalOperation(..)
  , ProposalStatus(..)
  , Protocol(..)
  , SimplePeriodInfo(..)
  , SimpleProposal(..)
  ) where

import Data.Aeson
import Data.Text
import Data.Time.Clock (UTCTime)
import GHC.Generics
import GHC.Int

import Node qualified

type Number = Int64

data Constants = Constants
  { blocksPerCycle    :: Maybe Int32
  , blocksPerVoting   :: Maybe Int32
  , timeBetweenBlocks :: Maybe Int32
  } deriving stock (Generic, Eq, Show)

data ProposalStatus
  = Active -- ^ the proposal in the active state
  | Accepted -- ^ the proposal was accepted
  | Rejected -- ^ the proposal was rejected due to too many "nay" ballots
  | Skipped -- ^ the proposal was skipped due to the quorum was not reached
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

textToProposalStatus
  :: Text
  -> ProposalStatus
textToProposalStatus = \case
  "active" -> Active
  "accepted" -> Accepted
  "rejected" -> Rejected
  "skipped" -> Skipped
  _ -> error "Unknown proposal staus"

proposalStatusToText
  :: ProposalStatus
  -> Text
proposalStatusToText = \case
  Active -> "active"
  Accepted -> "accepted"
  Rejected -> "rejected"
  Skipped -> "skipped"


instance FromJSON ProposalStatus where
  parseJSON v = textToProposalStatus <$> parseJSON v

instance ToJSON ProposalStatus where
  toJSON v = toJSON $ proposalStatusToText v

data PeriodStatus
  = PActive
  | PNoProposals
  | PNoQuorum
  | PNoSupermajority
  | PSuccess
  | PNoSingleWinner
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

textToPeriodStatus
  :: Text
  -> PeriodStatus
textToPeriodStatus = \case
  "active" -> PActive
  "no_proposals" -> PNoProposals
  "no_quorum" -> PNoQuorum
  "no_supermajority" -> PNoSupermajority
  "success" -> PSuccess
  "no_single_winner" -> PNoSingleWinner
  _ -> error "Unknown period staus"

periodStatusToText
  :: PeriodStatus
  -> Text
periodStatusToText = \case
  PActive -> "active"
  PNoProposals -> "no_proposals"
  PNoQuorum -> "no_quorum"
  PNoSupermajority -> "no_supermajority"
  PNoSingleWinner -> "no_single_winner"
  PSuccess -> "success"

instance FromJSON PeriodStatus where
  parseJSON v = textToPeriodStatus <$> parseJSON v

instance ToJSON PeriodStatus where
  toJSON v = toJSON (periodStatusToText v)

data PeriodType
  = Proposing     -- ^ Proposal phase (named `Proposing` to avoid name clashes with @Proposal@ datatype)
  | Exploration   -- ^ Exploration phase
  | Testing       -- ^ Testing phase
  | Promotion     -- ^ Promotion phase
  | Adoption      -- ^ Adoption phase
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

textToPeriodType
  :: Text
  -> PeriodType
textToPeriodType = \case
  "proposal" -> Proposing
  "exploration" -> Exploration
  "testing" -> Testing
  "cooldown" -> Testing
  "promotion" -> Promotion
  "adoption" -> Adoption
  _ -> error "Unknown period type"

periodTypeToText
  :: PeriodType
  -> Text
periodTypeToText = \case
  Proposing -> "proposal"
  Exploration -> "exploration"
  Testing -> "testing"
  Promotion -> "promotion"
  Adoption -> "adoption"

instance FromJSON PeriodType where
  parseJSON v = textToPeriodType <$> parseJSON v

instance ToJSON PeriodType where
  toJSON v = toJSON $ periodTypeToText v

data PeriodInfo = PeriodInfo
  { kind              :: PeriodType
  , firstLevel        :: Node.Level
  , lastLevel         :: Node.Level
  , startTime         :: UTCTime
  , endTime           :: UTCTime
  , status            :: Maybe PeriodStatus
  , epoch             :: Node.EpochIdx
  , yayBallots        :: Maybe Number
  , yayVotingPower    :: Maybe Node.VotingPower
  , nayBallots        :: Maybe Number
  , nayVotingPower    :: Maybe Node.VotingPower
  , passBallots       :: Maybe Number
  , passVotingPower   :: Maybe Node.VotingPower
  , ballotsQuorum     :: Maybe Float
  , supermajority     :: Maybe Float
  , totalBakers       :: Maybe Number
  , totalVotingPower  :: Maybe Node.VotingPower
  , index             :: Node.PeriodIdx
    -- An Epoch is a sequence of voting periods that comprises of one
    -- Tezos amendment. So it will always have one period (proposal) and
    -- possible contain further periods, util the proposal is rejected or
    -- activated.
  } deriving stock (Eq, Show, Generic)

data SimplePeriodInfo = SimplePeriodInfo
  { index :: Node.PeriodIdx
  , epoch :: Node.EpochIdx
  } deriving stock (Eq, Show, Generic)

data Protocol = Protocol
  { hash       :: Node.ProtocolHash
  , firstLevel :: Node.Level
  , lastLevel  :: Maybe Node.Level
  , constants  :: Constants
  } deriving (Show, Generic)

data Head = Head
  { level     :: Node.Level
  , hash      :: Node.BlockHash
  , protocol  :: Node.ProtocolHash
  , timestamp :: UTCTime
  } deriving (Eq, Show, Generic)

data Block = Block
  { level     :: Node.Level
  , timestamp :: UTCTime
  } deriving (Eq, Show, Generic)

data Delegate = Delegate
  { address :: Node.PkHash
  , alias   :: Maybe Text
  , logoUrl :: Maybe Text
  } deriving (Show, Eq, Generic)

instance Ord Delegate where
  compare d1 d2 = compare d1.address d2.address

data Ballot = Ballot
  { delegate      :: Delegate
  , vote          :: Node.Ballot
  , votingPower   :: Node.VotingPower
  , id            :: Number
  , hash          :: Node.OperationHash
  , timestamp     :: UTCTime
  , proposal      :: SimpleProposal
  , period        :: SimplePeriodInfo
  } deriving (Eq, Show, Generic)

data SimpleProposal = SimpleProposal
  { alias :: Maybe Text
  , hash  :: Node.ProposalHash
  } deriving (Show, Generic)

instance Ord SimpleProposal where
  compare a b = compare a.hash b.hash

instance Eq SimpleProposal where
  a == b = a.hash == b.hash

data EpochInfo = EpochInfo
  { index   :: Node.EpochIdx
  , periods :: [PeriodInfo]
  } deriving (Eq, Show, Generic)

data PeriodVoter = PeriodVoter
  { delegate    :: Delegate
  , votingPower :: Node.VotingPower
  } deriving (Eq, Show, Generic)

instance Ord PeriodVoter where
  compare p1 p2 = compare p1.delegate p2.delegate

data Operation = Operation
  { timestamp :: UTCTime
  , level     :: Node.Level
  , hash      :: Node.OperationHash
  , proposal  :: Maybe SimpleProposal
  , delegate  :: Delegate
  } deriving (Eq, Show, Generic)

data Proposal = Proposal
  { epoch       :: Node.EpochIdx
  , votingPower :: Node.VotingPower
  , hash        :: Node.ProposalHash
  , firstPeriod :: Node.PeriodIdx -- This field appear to be badly named in the indexer api.
  -- Because in the request to fetch a single proposal by hash, it returns the most recent
  -- proposal with the input hash. So the firstperiod will contain the most recent period where
  -- this proposal was injected. And not the very first period this proposal appeared. But in the
  -- response of proposal search request, which returns proposals in a list, this field contains the
  -- proper value of first period.
  , lastPeriod  :: Node.PeriodIdx
  , initiator   :: Delegate
  , status      :: ProposalStatus
  } deriving (Eq, Show, Generic)

data ProposalOperation = ProposalOperation
  { id          :: Number
  , hash        :: Node.OperationHash
  , proposal    :: SimpleProposal
  , timestamp   :: UTCTime
  , votingPower :: Node.VotingPower
  , delegate    :: Delegate
  , level       :: Node.Level
  } deriving (Eq, Show, Generic)

data VotingPeriod = VotingPeriod
  { totalBakers    :: Maybe Number
  , proposalsCount :: Maybe Number
  , yayBallots     :: Maybe Number
  , nayBallots     :: Maybe Number
  , yayRolls       :: Maybe Number
  , nayRolls       :: Maybe Number
  , passRolls      :: Maybe Number
  , passBallots    :: Maybe Number
  , totalRolls     :: Maybe Number
  } deriving (Show, Generic)

instance FromJSON Protocol where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Protocol where
  toJSON = genericToJSON defaultOptions

instance FromJSON Operation where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Operation where
  toJSON = genericToJSON defaultOptions

instance FromJSON SimpleProposal where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON SimpleProposal where
  toJSON = genericToJSON defaultOptions

instance FromJSON EpochInfo where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON EpochInfo where
  toJSON = genericToJSON defaultOptions

instance FromJSON Block where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Block where
  toJSON = genericToJSON defaultOptions

instance FromJSON Head where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Head where
  toJSON = genericToJSON defaultOptions

instance FromJSON Ballot where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Ballot where
  toJSON = genericToJSON defaultOptions

instance FromJSON Delegate where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Delegate where
  toJSON = genericToJSON defaultOptions

instance FromJSON PeriodInfo where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON PeriodInfo where
  toJSON = genericToJSON defaultOptions

instance FromJSON PeriodVoter where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON PeriodVoter where
  toJSON = genericToJSON defaultOptions

instance FromJSON ProposalOperation where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON ProposalOperation where
  toJSON = genericToJSON defaultOptions

instance FromJSON Proposal where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON Proposal where
  toJSON = genericToJSON defaultOptions

instance FromJSON VotingPeriod where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON VotingPeriod where
  toJSON = genericToJSON defaultOptions

instance FromJSON SimplePeriodInfo where
  parseJSON = genericParseJSON defaultOptions

instance ToJSON SimplePeriodInfo where
  toJSON = genericToJSON defaultOptions

instance ToJSON Constants where
  toJSON = genericToJSON defaultOptions

instance FromJSON Constants where
  parseJSON = genericParseJSON defaultOptions
