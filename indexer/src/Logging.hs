-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | A logging capability that simply wraps FastLogger.
module Logging
  ( LogLevel(..)
  , LoggingCap
  , MonadLoggingCap(..)
  , loggingCap
  ) where

import Control.Monad
import Control.Monad.IO.Class
import Monad.Capabilities
import System.Log.FastLogger

data LogLevel
  = LogDebug2
  | LogDebug
  | LogInfo
  deriving stock (Enum, Eq, Ord, Show)

data LoggingCap m = LoggingCap
  { _logMsg :: LogLevel -> LogStr -> m ()
  , _logDebug :: LogStr -> m ()
  , _logInfo  :: LogStr -> m ()
  }

$(makeCap ''LoggingCap)

loggingCap
  :: MonadIO m
  => FastLogger
  -> LogLevel
  -> CapImpl LoggingCap '[LoggingCap] m
loggingCap fl cutOffLevel = CapImpl $
  LoggingCap
    { _logMsg = \level m -> when (level >= cutOffLevel) $
          liftIO $ fl (m <> "\n")
    , _logDebug = \m -> logMsg LogDebug m
    , _logInfo = \m -> logMsg LogInfo m
    }

