-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | Common stuff that can be imported from elsewhere in this project.
module Common
  ( FromIntegral(..)
  , MonadIO
  , liftIO
  , throwRE
  , pass
  , showt
  ) where

import Control.Exception (Exception, throwIO)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Text (Text, pack)
import GHC.Int
import Universum (pass)

class FromIntegral a b where
  fromIntegralSafe :: (Integral a, Integral b) => a -> b
  fromIntegralSafe = fromIntegral

-- Declare some safe instances, taking into account the sizes of source type
-- and target type.
instance FromIntegral a a
instance FromIntegral Int32 Int64
instance FromIntegral Int Int64
instance FromIntegral Int32 Int
instance FromIntegral Int64 Integer

newtype RuntimeError = RuntimeError Text
  deriving stock Show

instance Exception RuntimeError

-- | Throws an runtime error.
throwRE
  :: MonadIO m
  => Text
  -> m a
throwRE = liftIO . throwIO . RuntimeError

-- | Variant of @show@ function that returns a Text instead of string.
-- To be used instead of polymorphic @show@ from universum to save us from
-- providing type annotations when usage site is also polymorhic.
showt
  :: Show s
  => s
  -> Text
showt = pack . show
