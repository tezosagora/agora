-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Config
  ( Config(..)
  , readConfig
  ) where

-- | Infra to handle CLI/environment configuration management.
import Common
import Data.ByteString (ByteString)
import Data.Maybe
import Data.Text as T
import Data.Text.Encoding
import Options.Generic
import Servant.Client
import System.Environment

data Config = Config
  { connectString :: ByteString
  , nodes :: [BaseUrl]
  , serverPort :: Int
  , nodeStatsPort :: Int
  } deriving stock Show

data CmdLineArgs = CmdLineArgs
  { port :: Text <!> "4040"
  , statsPort :: Text <!> "7720"
  , node :: [Text]
  , database :: Text <!> "postgresql://postgres@localhost/tzidx"
  } deriving stock (Generic, Show)

instance ParseRecord CmdLineArgs

-- | This function is a helper to lookup environment values and convert
-- evn strings to domain specific types, while also accepting a default
-- for the target value. The second argument is a callback that tells
-- how strings should be converted into the target value.
--
-- The third argument specifies default value. It can be either a string,
-- upon which the second argument will be invoked with it to make the target
-- type, or the actual target value itself, upon which it is just returned.
lookupEnv'
  :: String
  -> (Text -> Maybe a)
  -> Either Text a
  -> IO a
lookupEnv' key conv def = lookupEnv key >>= \case
  Just a -> pure $ fromMaybe (error "Parsing env var failed") (conv $ T.pack a)
  Nothing -> pure $ case def of
    Right a -> a
    Left txt -> fromMaybe (error "Parsing default env var failed") (conv txt)

-- | Reads config from environment values or command line arguments.
-- Environment values override command line arguments. This behavior is
-- mostly to aid docker based deployments, where the environment can be tweaked
-- more easily than the command that is invoked during deployment.
readConfig :: IO Config
readConfig = do
  CmdLineArgs {..} <- getRecord "Agora indexer config"
  serverPort <- lookupEnv' "TZ_INDEXER_LISTEN_PORT"
    (Just . read . T.unpack) (Left $ unDefValue port)
  statsServerPort <- lookupEnv' "TZ_INDEXER_STATS_PORT"
    (Just . read . T.unpack) (Left $ unDefValue statsPort)
  dbConnStr <- lookupEnv' "TZ_INDEXER_DATABASE" Just (Left $ unDefValue database)
  nodes <- case mapM (parseBaseUrl . unpack) node of
    Just cliNodes -> lookupEnv' "TZ_INDEXER_NODES"
      (mapM (parseBaseUrl . unpack) . T.split (== ',')) (Right cliNodes)
    Nothing -> throwRE "Cannot parse node urls from command line"
  pure $ Config (encodeUtf8 dbConnStr) nodes serverPort statsServerPort
