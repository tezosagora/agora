-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | Module that defines infra to run actions that require various combination
-- of capabilities.
module Caps
  ( ApiCaps
  , ApiM
  , NodeCaps
  , NodeM
  , SyncCaps
  , SyncM
  , runApiM
  , runSyncM
  , withDeps
  ) where

import Control.Monad.Reader
import Control.Concurrent.STM.TMVar
import Database.PostgreSQL.Simple
import Monad.Capabilities
import System.Log.FastLogger
import UnliftIO.Async

import Common
import Config
import Database.Caps
import Logging
import Node
import ServerPool
import StatsServer

type SyncCaps = '[NodeCap, ServerPoolCap, LoggingCap, DbCap]
type ApiCaps = '[LoggingCap, DbCap]
type NodeCaps = '[LoggingCap, ServerPoolCap, NodeCap]

type SyncM = CapsT SyncCaps IO
type ApiM = CapsT ApiCaps IO
type NodeM = CapsT NodeCaps IO

withDeps
  :: (Connection -> FastLogger -> Config -> IO a)
  -> IO a
withDeps fn = do
  config <- readConfig
  connection <- connectPostgreSQL $ connectString config
  withFastLogger (LogStdout defaultBufSize) $ \fl -> fn connection fl config

apiMCaps
  :: Connection
  -> FastLogger
  -> Capabilities ApiCaps IO
apiMCaps connection logger = buildCaps $
  AddCap (loggingCap logger LogDebug) $
  AddCap (dbCap connection) $
  BaseCaps emptyCaps

syncMCaps
  :: HeadBlockCache
  -> ServerPoolRefs
  -> Connection
  -> FastLogger
  -> Capabilities SyncCaps IO
syncMCaps headBlockCache serverInfos connection logger = buildCaps $
  AddCap (nodeCap headBlockCache) $
  AddCap (serverPoolCap serverInfos) $
  BaseCaps (apiMCaps connection logger)

runSyncM'
  :: Config
  -> Connection
  -> FastLogger
  -> SyncM a
  -> IO a
runSyncM' config connection loggr a = do
  serverInfos <- mkServerInfos config.nodes
  headBlockCache <- newTMVarIO Nothing
  -- start a http server to serve node statistics in a thread
  -- before starting the syncing process.
  withAsync (serveStats config.nodeStatsPort serverInfos.serverInfoRef) $ \_ ->
    runReaderT a (syncMCaps headBlockCache serverInfos connection loggr)

runApiM'
  :: Connection
  -> FastLogger
  -> ApiM a
  -> IO a
runApiM' connection loggr a = let
  in runReaderT a (apiMCaps connection loggr)

runApiM
  :: ApiM a
  -> IO a
runApiM a = withDeps (\c l _ -> runApiM' c l a)

runSyncM
  :: SyncM a
  -> IO a
runSyncM a = do
  withDeps (\conn logger config -> do
    runSyncM' config conn logger $ do
      logDebug $ toLogStr $ "Using config:" <> showt config
      a)
