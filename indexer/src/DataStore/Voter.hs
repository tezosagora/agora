-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module DataStore.Voter
  ( getVoter
  , getVoterWithAddress
  , getPeriodVoter
  , getVotersForPeriodWithVotingPower
  , insertPeriodVoter
  , insertVoter
  ) where

import Data.Text (Text)

import Common
import DataStore.Common
import Database.Models.Blocks
import Database.Models.PeriodVoters
import qualified Database.Models.PeriodVoters as PeriodVoters
import Database.Models.Voters
import qualified Database.Models.Voters as Voters
import qualified Node

{-# ANN module ("HLint: ignore Redundant id" :: Text) #-}

getVoter
  :: (MonadIO m, HasCap DbCap caps)
  => Voters.VoterId
  -> CapsT caps m Voter
getVoter voterId = let
  query = do
    vt <- selectTable votersTable
    where_ (vt.id .== toFields voterId)
    pure vt
  in withConnection $ \conn -> listToOne $ runSelect conn query

getVoterWithAddress
  :: (MonadIO m, HasCap DbCap caps)
  => Node.PkHash
  -> CapsT caps m (Maybe Voter)
getVoterWithAddress voterAddr = let
  query = do
    vt <- selectTable votersTable
    where_ (vt.voterAddress .== toFields voterAddr)
    pure vt
  in withConnection $ \conn -> liftIO $ listToMaybe <$> runSelect conn query

getPeriodVoter
  :: (MonadIO m, HasCap DbCap caps)
  => Node.PkHash
  -> Node.PeriodIdx
  -> CapsT caps m (Maybe PeriodVoter)
getPeriodVoter voterAddress periodIdx = let
  query = do
    periodVoter <- selectTable periodVotersTable
    vt <- selectTable votersTable
    where_ (vt.voterAddress .== toFields voterAddress
      .&& vt.id .== periodVoter.voterId  .&& periodVoter.periodIdx .== toFields periodIdx)
    pure periodVoter
  in withConnection $ \conn -> liftIO $ listToMaybe <$> runSelect conn query

getVotersForPeriodWithVotingPower
  :: (MonadIO m, HasCap DbCap caps)
  => Node.PeriodIdx
  -> Maybe Int
  -> Maybe Int
  -> CapsT caps m [(Voter, Node.VotingPower)]
getVotersForPeriodWithVotingPower periodIdx mOffset mLimit = let
  query = do
    vt <- selectTable votersTable
    pv <- selectTable periodVotersTable
    where_ (vt.id .== pv.voterId .&& pv.periodIdx .== toFields periodIdx)
    pure (vt, pv.votingPower)
  in withConnection $
    \conn -> liftIO (runSelect conn (applyOffsetLimit mOffset mLimit (desc ((.id) . fst)) query))

insertVoter
  :: (MonadIO m, HasCap DbCap caps)
  => BlockId
  -> Node.PkHash
  -> CapsT caps m VoterId
insertVoter sourceBlockId voterAddress
  = withConnection $ \conn ->
        listToOne $
          runInsert conn $
            Insert votersTable
              [toFields (Voter (Nothing :: Maybe VoterId)
                voterAddress sourceBlockId )] (rReturning Voters.id) Nothing

insertPeriodVoter
  :: (MonadIO m, HasCap DbCap caps)
  => BlockId
  -> VoterId
  -> Node.PeriodIdx
  -> Node.VotingPower
  -> CapsT caps m PeriodVoterId
insertPeriodVoter sourceBlockId voterId periodIdx votingPower =
  withConnection $ \conn ->
        listToOne $
          runInsert conn $
            Insert periodVotersTable
              [toFields
                (PeriodVoter
                    (Nothing :: Maybe PeriodVoterId)
                      voterId periodIdx votingPower sourceBlockId )
              ] (rReturning PeriodVoters.id) Nothing
