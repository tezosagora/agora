-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module DataStore.Block
  ( cacheBlock
  , deleteBlock
  , fetchBlockFromCache
  , getBlock
  , getBlockCount
  , getBlockWithId
  , getLastActiveBlockForProposalInEpoch
  , getLastCommitedBlock
  , getLastCommitedBlockInLevelRange
  , insertBlock
  , removeBlockFromCache
  ) where

import Prelude hiding (max, min)

import GHC.Int
import Data.Text (Text)

import Common
import qualified Database.Models.BlockCache as BlockCache
import Database.Models.Blocks
import Database.Models.Proposals
import Database.Models.ProposalStatus
import Database.Models.NetworkState
import DataStore.Common
import qualified Node

{-# ANN module ("HLint: ignore Redundant id" :: Text) #-}

getBlock
  :: (MonadIO m, HasCap DbCap caps)
  => Node.BlockSpec
  -> CapsT caps m (Maybe Block)
getBlock bs = withConnection $ \conn -> let
  baseQuery = do
    block <- selectTable blocksTable
    case bs of
      Node.LevelSpec l -> where_ (block.level .== toFields l)
      Node.HashSpec h  -> where_ (block.hash .== toFields h)
      Node.NodeHead    -> pass
    pure block
  query = case bs of
    Node.NodeHead -> orderBy (desc blockTime) baseQuery
    _             -> baseQuery
  in liftIO $ listToMaybe <$> runSelect conn (limit 1 query)

getLastCommitedBlock
  :: (MonadIO m, HasCap DbCap caps)
  => CapsT caps m (Maybe Block)
getLastCommitedBlock = withConnection $ \conn ->
  liftIO $ listToMaybe <$> runSelect conn
    (limit 1 $ orderBy (desc blockTime) $ selectTable blocksTable)

getLastCommitedBlockInLevelRange
  :: (MonadIO m, HasCap DbCap caps)
  => Node.Level
  -> Node.Level
  -> CapsT caps m (Maybe Block)
getLastCommitedBlockInLevelRange startLevel endLevel = withConnection $
  \conn -> do
  let
    query = do
      block <- selectTable blocksTable
      where_ $ (block.level .>= toFields startLevel) .&& (block.level .<= toFields endLevel)
      pure block
  liftIO $ listToMaybe <$> runSelect conn
    (limit 1 $ orderBy (desc blockTime) query)

getBlockWithId
  :: (MonadIO m, HasCap DbCap caps)
  => BlockId
  -> CapsT caps m Block
getBlockWithId blockId = withConnection $ \conn ->
  listToOne $ runSelect conn (limit 1 $ do
    bs <- selectTable blocksTable
    where_ (bs.id .== toFields blockId)
    pure bs)

getLastActiveBlockForProposalInEpoch
  :: (MonadIO m, HasCap DbCap caps)
  => Node.ProposalHash
  -> Node.EpochIdx
  -> CapsT caps m (Maybe Node.Level)
getLastActiveBlockForProposalInEpoch proposalHash epoch = let
  query = limit 1 $ aggregate min $ do
    proposal <- selectTable proposalsTable
    ps <- selectTable proposalStatusTable
    blk <- selectTable blocksTable
    ns <- selectTable networkStateTable
    where_ (proposal.proposalHash .== toFields proposalHash)
    where_ (ps.proposalId .== proposal.id)
    where_ (ps.sourceBlockId .== blk.id)
    where_ (ns.periodIdx .== proposal.periodIdx)
    where_ (ps.status ./= toFields Node.Active)
    where_ (ns.epoch .== toFields epoch)
    pure blk.level
  in withConnection $ \conn -> do
    liftIO $ listToMaybe <$> runSelect conn query  >>= \case
      Just l -> pure $ Just (l - 1) -- Subtract one level to get the block just before this one where the status switched.
      Nothing -> pure Nothing

insertBlock
  :: (MonadIO m, HasCap DbCap caps)
  => Node.Block
  -> CapsT caps m Block
insertBlock block = withConnection $ \conn -> listToOne $
  runInsert conn $
    Insert blocksTable [toFields (blockToDbBlock block)] (rReturning Prelude.id) Nothing

getBlockCount
  :: (MonadIO m, HasCap DbCap caps)
  => CapsT caps m Node.Level
getBlockCount = let
  query =
    aggregate max (level <$> selectTable blocksTable)
  in withConnection $ \conn -> listToOne $ runSelect conn query

deleteBlock
  :: (MonadIO m, HasCap DbCap caps)
  => Block
  -> CapsT caps m Int64
deleteBlock targetBlock = withConnection $ \conn -> liftIO $ runDelete conn $
  Delete blocksTable (\blk -> blk.id .== (toFields targetBlock.id)) rCount

fetchBlockFromCache
  :: (MonadIO m, HasCap DbCap caps) => Node.BlockSpec -> CapsT caps m (Maybe Node.Block)
fetchBlockFromCache blockSpec = withConnection $ \conn -> liftIO $ do
  let
    query = do
      q <- selectTable BlockCache.blockCacheTable
      case blockSpec of
        Node.LevelSpec l -> where_ (q.level .== toFields l)
        Node.HashSpec h -> where_ (q.hash .== toFields h)
        Node.NodeHead -> where_ (toFields False)
      pure q.content
  listToMaybe <$> runSelect conn query

cacheBlock
  :: (MonadIO m, HasCap DbCap caps) => Node.Block -> CapsT caps m Int64
cacheBlock b = withConnection $ \conn ->
  liftIO (runInsert conn $
    Insert BlockCache.blockCacheTable
      [BlockCache.BlockCache (toFields b.header.level)
        (toFields b.hash) (toFields $ encode b)] rCount Nothing)

removeBlockFromCache
  :: (MonadIO m, HasCap DbCap caps) => Node.BlockHash -> CapsT caps m Int64
removeBlockFromCache bHash = withConnection $ \conn ->
  liftIO (runDelete conn $
    Delete BlockCache.blockCacheTable (\cb -> cb.hash .== toFields bHash) rCount)
