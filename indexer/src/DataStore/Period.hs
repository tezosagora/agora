-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module DataStore.Period
  ( getPeriodForBlock
  , getPeriodStatus
  , getPeriodsInEpoch
  , insertPeriodStatus
  ) where

import Data.Text (Text)

import Common
import Database.Models.Blocks
import Database.Models.NetworkState
import Database.Models.PeriodStatus
import DataStore.Common
import Logging
import qualified Node
import Prelude hiding (max)

{-# ANN module ("HLint: ignore Redundant id" :: Text) #-}

getPeriodForBlock
  :: (MonadIO m, HasCap DbCap caps)
  => Node.Level
  -> CapsT caps m Node.PeriodIdx
getPeriodForBlock level = let
  query = limit 1 $ aggregate max $ do
    ns <- selectTable networkStateTable
    blk <- selectTable blocksTable
    where_ (ns.sourceBlockId .== blk.id)
    where_ (blk.level .<= toFields level)
    pure ns.periodIdx
  in withConnection $ \conn -> listToOne $ runSelect conn query

getPeriodStatus
  :: (MonadIO m, HasCap DbCap caps)
  => Node.PeriodIdx
  -> CapsT caps m Node.PeriodStatus
getPeriodStatus periodIdx = let
  query = fst <$> limit 1 (orderBy (desc snd) $ do
    ps <- selectTable periodStatusTable
    blocks <- selectTable blocksTable
    where_ (ps.periodIdx .== toFields periodIdx)
    where_ (blocks.id .== ps.sourceBlockId)
    pure (ps.status, blocks.blockTime))
  in withConnection $ \conn -> listToOne $ runSelect conn query

getPeriodsInEpoch
  :: (MonadIO m, HasCap DbCap caps)
  => Node.EpochIdx
  -> CapsT caps m [Node.PeriodIdx]
getPeriodsInEpoch epochIdx = let
  query = aggregate groupBy $ orderBy (asc Prelude.id) $ do
    vt <- selectTable networkStateTable
    where_ (vt.epoch .== toFields epochIdx)
    pure vt.periodIdx
  in withConnection $
    \conn -> liftIO (runSelect conn query)

insertPeriodStatus
  :: (MonadIO m, HasCap LoggingCap caps, HasCap DbCap caps)
  => BlockId
  -> Node.PeriodIdx
  -> Node.PeriodStatus
  -> CapsT caps m PeriodStatus
insertPeriodStatus sourceBlockId periodIdx status
  = withConnection $ \conn -> listToOne $ runInsert conn $
      Insert periodStatusTable
        [toFields
          (PeriodStatus
            (Nothing :: Maybe Int) periodIdx status sourceBlockId)
        ] (rReturning Prelude.id) Nothing
