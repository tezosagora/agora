-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module DataStore.Epoch
  ( getEpochForPeriod
  , getAllEpochs
  ) where

import Common
import DataStore.Common

import Database.Models.NetworkState
import qualified Node

getEpochForPeriod
  :: (MonadIO m, HasCap DbCap caps)
  => Node.PeriodIdx
  -> CapsT caps m Node.EpochIdx
getEpochForPeriod periodIdx = let
  query = limit 1 $ do
    ns <- selectTable networkStateTable
    where_ (ns.periodIdx .== toFields periodIdx)
    pure ns.epoch
  in withConnection $ \conn -> listToOne $ runSelect conn query

getAllEpochs
  :: (MonadIO m, HasCap DbCap caps)
  => CapsT caps m [Node.EpochIdx]
getAllEpochs = let
  query = aggregate groupBy $ orderBy (asc Prelude.id) $  do
    vt <- selectTable networkStateTable
    pure vt.epoch
  in withConnection $
    \conn -> liftIO (runSelect conn query)
