-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

module DataStore.Protocol
  ( getProtocol
  , getProtocolForBlock
  , getProtocols
  , insertBlocksToProtocol
  , insertProtocol
  ) where

import Data.Text (Text)

import Common
import DataStore.Common
import Database.Models.Blocks
import Database.Models.Protocols
import Database.Models.BlocksToProtocol
import Database.Models.BlocksToProtocol qualified as BlocksToProtocol
import Node qualified

{-# ANN module ("HLint: ignore Redundant id" :: Text) #-}

getProtocols
  :: (MonadIO m, HasCap DbCap caps)
  => Maybe Int
  -> Maybe Int
  -> CapsT caps m [(Node.ProtocolHash, Node.Level, Maybe Node.Level, Node.RawConstants)]
getProtocols mOffset mLimit = do
  let
    query = do
      (a, b, c, _) <- orderBy (asc (\(_, _, _, bt) -> bt)) $ do
        prt <- selectTable protocolsTable
        bs <- selectTable blocksTable
        where_ (prt.sourceBlockId .== bs.id)
        pure (prt.protocolHash, bs.level, prt.constants, bs.blockTime)
      pure (a, b, c)

  protocols <- withConnection $ \conn ->
    liftIO (runSelect conn (applyOffsetLimit mOffset mLimit mempty query))
  -- Fill last level for each protocol using the first level of the next protocol - 1.
  pure $ zipWith fillLastLevel protocols
      (drop 1 ((\(_, b, _) -> Just b) <$> protocols) ++ [Nothing])
  where
    fillLastLevel (ph, lv, cst) (Just lv') = (ph, lv, Just $ lv' - 1, cst)
    fillLastLevel (ph, lv, cst) Nothing    = (ph, lv, Nothing , cst)

getProtocolForBlock
  :: (MonadIO m, HasCap DbCap caps)
  => Block
  -> CapsT caps m Protocol
getProtocolForBlock targetBlock = let
  query = fmap snd $ limit 1 $ orderBy (desc (blockTime . fst)) $ do
    block <- selectTable blocksTable
    blocksToProtocol <- selectTable blocksToProtocolsTable
    protocol <- selectTable protocolsTable
    where_ (blocksToProtocol.blockId .== block.id
      .&& blocksToProtocol.protocolId .== protocol.id
      .&& block.blockTime .<= (toFields targetBlock.blockTime))
    pure (block, protocol)
  in withConnection $ \conn -> listToOne $ runSelect conn query

getProtocol
  :: (MonadIO m, HasCap DbCap caps)
  => Node.ProtocolHash
  -> CapsT caps m (Maybe Protocol)
getProtocol protocolHash = let
  query = do
    pt <- selectTable protocolsTable
    where_ (pt.protocolHash .== toFields protocolHash)
    pure pt
  in withConnection $ \conn -> liftIO $ listToMaybe <$> runSelect conn query

insertProtocol
  :: (MonadIO m, HasCap DbCap caps)
  => Node.ProtocolHash
  -> Node.RawConstants
  -> BlockId
  -> CapsT caps m Protocol
insertProtocol protocolHash constants sourceBlockId = withConnection $ \conn ->
  listToOne $ runInsert conn $ Insert protocolsTable
    [toFields (Protocol
        (Nothing :: Maybe ProtocolId) protocolHash constants sourceBlockId)]
    (rReturning Prelude.id) Nothing

insertBlocksToProtocol
  :: (MonadIO m, HasCap DbCap caps)
  => BlockId
  -> ProtocolId
  -> CapsT caps m ProtocolId
insertBlocksToProtocol blockId protocolId = withConnection $ \conn ->
  listToOne (runInsert conn $ Insert blocksToProtocolsTable
    [toFields (BlocksToProtocol
      (Nothing :: Maybe BlocksToProtocolId) blockId protocolId)]
        (rReturning BlocksToProtocol.id) Nothing)
