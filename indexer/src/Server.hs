-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | The API server.
module Server
  ( serveApi
  ) where

import Control.Monad.Catch hiding (Handler)
import Control.Monad.IO.Class
import Network.Wai.Handler.Warp (run)
import Servant (Proxy(..), ServerError, ServerT, ToServant, hoistServer, throwError)
import Servant.Server (Application, Handler, serve)
import Servant.Server.Generic (AsServerT, genericServerT)
import System.Log.FastLogger

import Api.Api
import qualified Api.Handlers as Handlers
import Caps
import Config
import Logging

api :: Proxy IndexerApi
api = Proxy

type ApiHandlers m = ToServant IndexerEndpoints (AsServerT m)

indexerHandlers :: ApiHandlers ApiM
indexerHandlers = genericServerT IndexerEndpoints
  { neLevel_ = Handlers.getLevel
  , neHead_ = Handlers.getHead
  , neBallotsForProposal_ = Handlers.getBallots
  , neVoters_ = Handlers.getVotersForPeriod
  , neBlockCount_ = Handlers.getBlockCount
  , neGetProposal_ = Handlers.getProposal
  , neGetProposals_ = Handlers.getProposalsForEpoch
  , neProposalOperations_ = Handlers.getProposalOperations
  , neProtocols_ = Handlers.getProtocols
  , neEpoch_ = Handlers.getEpoch
  , neEpochs_ = Handlers.getEpochs
  , neVersion = Handlers.getVersion
  }

handlerServer :: ServerT IndexerApi Handler
handlerServer = hoistServer (Proxy :: Proxy IndexerApi) readerToHandler indexerHandlers
 where
  readerToHandler :: ApiM x -> Handler x
  readerToHandler r = catch (liftIO $ runApiM r) servantErrorHandler

  servantErrorHandler :: ServerError -> Handler x
  servantErrorHandler err = throwError err

apiApp :: Application
apiApp = serve api handlerServer

serveApi :: IO ()
serveApi = do
  config <- readConfig
  runApiM $ logInfo $ toLogStr $ "Running on port: " <> (show config.serverPort)
  run config.serverPort apiApp
