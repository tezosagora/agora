-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | Application's interface to the database. We try to do all the database
-- access through functions defined in this module and its submodules.
module DataStore
  ( BallotsQueryResult(..)
  , cacheBlock
  , clearDb
  , deleteBlock
  , fetchBlockFromCache
  , getAllEpochs
  , getBallotsForProposal
  , getBallotsForProposalWithVotingPower
  , getBlock
  , getBlockCount
  , getBlockWithId
  , getEpochForPeriod
  , getLastActiveBlockForProposalInEpoch
  , getLastCommitedBlock
  , getLastCommitedBlockInLevelRange
  , getNetworkState
  , getNetworkStateForPeriod
  , getPeriodForBlock
  , getPeriodStatus
  , getPeriodVoter
  , getPeriodsInEpoch
  , getProposal
  , getProposalOperationForProposal
  , getProposalOperationsForPeriod
  , getProposalOperationsForProposal
  , getProposalPeriodForEpoch
  , getProposalStatus
  , getProposalsForPeriod
  , getProposalsForPeriodWithUpvotes
  , getProposalsWithStatusesFor
  , getProtocol
  , getProtocolForBlock
  , getProtocols
  , getRecentProposalWithHash
  , getTotalVotingPower
  , getVoter
  , getVoterWithAddress
  , getVotersForPeriodWithVotingPower
  , getVotingPowerForProposal
  , insertBallotOperation
  , insertBlock
  , insertBlocksToProtocol
  , insertChainId
  , insertNetworkState
  , insertPeriodStatus
  , insertPeriodVoter
  , insertProposal
  , insertProposalOperation
  , insertProposalStatus
  , insertProtocol
  , insertVoter
  , removeBlockFromCache
  ) where

import Control.Monad.IO.Class
import Data.Int
import Opaleye

import Database.Caps
import qualified Database.Models.Blocks as Blocks
import qualified Database.Models.BlockCache as BlockCache
import Monad.Capabilities

import DataStore.Ballot
import DataStore.Block
import DataStore.ChainId
import DataStore.Epoch
import DataStore.NetworkState
import DataStore.Period
import DataStore.Proposal
import DataStore.Protocol
import DataStore.Voter
import DataStore.VotingPower

clearDb
  :: (MonadIO m, HasCap DbCap caps) => CapsT caps m Int64
clearDb = withConnection $ \conn -> liftIO $ do
  _ <- runDelete conn $ Delete Blocks.blocksTable (const (toFields True)) rCount
  runDelete conn $ Delete BlockCache.blockCacheTable (const (toFields True)) rCount
