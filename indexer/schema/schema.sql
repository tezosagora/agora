-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

CREATE TABLE blocks
  ( id serial primary key
  , hash text NOT NULL
  , chain_id text
  , level int not null
  , predecessor_hash text not NUll
  , blocktime timestamp with time zone not null
  , UNIQUE(hash)
  , UNIQUE(level)
  );

CREATE INDEX blocks_sorted_index ON blocks (blocktime DESC);

CREATE TABLE chain_id
  ( id int primary key
  , chain_id text NOT NULL
  , source_block_id int references blocks(id) on delete cascade not null
  , UNIQUE(chain_id)
  );

ALTER TABLE blocks ADD CONSTRAINT "blocks_chain_id" FOREIGN KEY (chain_id) REFERENCES chain_id(chain_id) deferrable initially deferred;

create table network_state
  ( id serial primary key
  , period_idx int not null
  , period_kind text not null
  , epoch int not null
  , min_quorum int
  , source_block_id int references blocks(id) on delete cascade not null
  , UNIQUE(period_idx)
  );

create index ns_epoch_index on network_state (epoch);
create index ns_source_block_index on network_state (source_block_id);

create table proposals
  ( id serial primary key
  , proposal_hash text not null
  , period_idx int not null
  , source_block_id int references blocks(id) on delete cascade not null
  , UNIQUE(proposal_hash, period_idx)
  );
create index proposals_source_block_index on proposals (source_block_id);

create table proposal_status
  ( id serial primary key
  , proposal_id int references proposals(id) on delete cascade not null
  , status text not null
  , source_block_id int references blocks(id) on delete cascade not null
  );
create index proposal_status_source_block_index on proposal_status (source_block_id);
create index proposal_status_proposal_id_index on proposal_status (proposal_id);

create table period_status
  ( id serial primary key
  , period_idx int references network_state(period_idx) on delete cascade not null
  , status text not null
  , source_block_id int references blocks(id) on delete cascade not null
  );
create index period_status_source_block_index on period_status (source_block_id);
create index period_status_period_idx on period_status (period_idx);

create table voters
  ( id serial primary key
  , voter_address text not null
  , source_block_id int references blocks(id) on delete cascade not null
  , UNIQUE(voter_address)
  );
create index voters_source_block_index on voters (source_block_id);
create index voters_voter_index on voters (voter_address);

create table period_voters
  ( id serial primary key
  , voter_id int references voters(id) not null
  , period_idx int not null
  , voting_power bigint not null
  , source_block_id int references blocks(id) on delete cascade not null
  , UNIQUE(voter_id, period_idx)
  );
create index voters_period_index on period_voters (period_idx);
create index period_voters_source_block_index on period_voters (source_block_id);

create table proposal_operations
  ( id serial primary key
  , proposal_id int references proposals(id) not null
  , period_idx int references network_state(period_idx) on delete cascade not null
  , operation_hash text not null
  , period_voter_id int not null references period_voters(id)
  , source_block_id int references blocks(id) on delete cascade not null
  );
create index proposal_operations_source_block_index on proposal_operations(source_block_id);
create index proposal_operations_period_voter_index on proposal_operations(period_voter_id);

create table ballot_operations
  ( id serial primary key
  , proposal_id int references proposals(id) not null
  , period_idx int references network_state(period_idx) on delete cascade not null
  , operation_hash text not null
  , period_voter_id int references period_voters(id) not null
  , ballot text not null
  , source_block_id int references blocks(id) on delete cascade  not null
  );
create index ballots_source_block_index on ballot_operations (source_block_id);
create index ballots_period_voter_index on ballot_operations (period_voter_id);
create index ballots_proposal_id_index on ballot_operations (proposal_id);

create table protocols
  ( id serial primary key
  , protocol_hash text not null
  , constants jsonb not null
  , source_block_id int references blocks(id) on delete cascade not null
  , UNIQUE(protocol_hash)
  );
create index protocols_protocol_index on protocols (protocol_hash);
create index protocols_source_block_index on protocols (source_block_id);

create table blocks_to_protocols
  ( id serial primary key
  , block_id int references blocks(id) on delete cascade not null
  , protocol_id int references protocols(id) not null
  , UNIQUE(block_id)
  );

create table if not exists block_cache
  ( level int not null primary key
  , hash text not null
  , content jsonb not null
  , UNIQUE(hash)
  );
create index if not exists block_cache_index on block_cache (hash);
