#!/bin/bash
#
# SPDX-FileCopyrightText: 2023 Tezos Commons
#
# SPDX-License-Identifier: AGPL-3.0-or-later

pg_ctl start
createdb tzidx
cd /home/tzidx/agora/indexer
cp `cabal list-bin indexer-test` .
export TZ_INDEXER_DATABASE=postgresql://tzidx@localhost/tzidx && ./indexer-test
