-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | A program to generate configurable list of blocks that represent
-- the block chain state, to test the indexer against.
module Indexer.ChainGenerator
  ( AdoptionPeriodConfig(..)
  , BallotsConfig(..)
  , ChainState(..)
  , ChainStateRef
  , ExplorationPeriodConfig(..)
  , PeriodConfig(..)
  , PromotionPeriodConfig(..)
  , ProposalPeriodConfig(..)
  , TestingPeriodConfig(..)
  , atomicReadChainState
  , emptyChainState
  , emptyConstants
  , generateBlocksForConfig
  , mergeChainStateAt
  , generateChain
  , printChain
  , levelsPerPeriod
  , rawConstants
  , rawConstants'
  , startBlockForPeriod
  ) where

import Control.Concurrent.STM.TVar (TVar, readTVarIO)
import Control.Monad (forM, foldM, when)
import Data.Aeson (Value(..), toJSON)
import Data.Map.Strict qualified as Map
import Data.Maybe
import Data.Text (pack)
import Data.Time
import Data.Vector qualified as V
import System.Random

import Common
import Node

genesisBlockHash :: BlockHash
genesisBlockHash = Hash "BlockGenesis"

genesisBlock :: RawBlock
genesisBlock = RawBlock
  { hash = genesisBlockHash
  , chain_id = mainChainId
  , header = Header 0 (Hash "GenesisPredecessor") (read "2000-01-01 00:00:00") 0
  , protocol = defaultProtocolHash
  , metadata = Nothing
  , operations = []
  }

genesisBlock1 :: RawBlock
genesisBlock1 = RawBlock
  { hash = Hash "BlockGenesisSucc1"
  , chain_id = mainChainId
  , header = Header 1 genesisBlockHash (read "2000-01-01 00:00:30") 0
  , protocol = defaultProtocolHash
  , metadata = Nothing
  , operations = []
  }

genesisBlock2 :: RawBlock
genesisBlock2 = RawBlock
  { hash = Hash "BlockGenesisSucc2"
  , chain_id = mainChainId
  , header = Header 2 (Hash "GenesisSucc1") (read "2000-01-01 00:01:00") 0
  , protocol = defaultProtocolHash
  , metadata = Just $ Node.RMDCurrent $ RawMetadataCurrent
      { voting_period_info = VotingPeriod $ VotingPeriodInfo 0 Proposal
      }
  , operations = []
  }

-- Here we have couple of blocks after the genesis block so that when we
-- trigger the fork, the indexer don't have to back all the way back to the
-- genesis block (esentially clearing the database).
emptyChainState :: ChainState
emptyChainState = ChainState
  { currentBlocks =
      Map.fromList $ (\x -> (x.hash, x)) <$> [genesisBlock, genesisBlock1, genesisBlock2]
  , currentLevels = V.fromList $ (.hash) <$> [genesisBlock, genesisBlock1, genesisBlock2]
  , periodVoters = mempty
  , currentTime = read "2000-01-01 00:02:00"
  , contextMap = mempty
  }

type PeriodVotersMap = Map.Map PeriodIdx (Map.Map PkHash RawVoter)

data ChainState = ChainState
  { currentBlocks :: Map.Map BlockHash RawBlock
  , currentLevels :: V.Vector BlockHash
  , periodVoters :: PeriodVotersMap
  , currentTime :: UTCTime
  , contextMap :: Map.Map Level RawConstants -- Maps block hash to context that was activated at that point.
  } deriving Show

printChain :: ChainState -> IO ()
printChain chainState = do
  V.mapM_ printOneBlock chainState.currentLevels
  putStrLn "Period voters..."
  where
    printOneBlock :: BlockHash -> IO ()
    printOneBlock bh = case Map.lookup bh chainState.currentBlocks of
      Just b -> putStrLn $ show b.header.level
      Nothing -> error "Block not found"

-- | Insert the first chain state into the second chainstate
-- at the merge point. In otherwords, roughly this is,
-- mergeChainStateAt c1 c2 offset = take offset c1 + drop offset c2
-- The map of period voters is also merged from the two chain states
-- taking them for respective periods from each chain states.
mergeChainStateAt
  :: ChainState
  -> ChainState
  -> PeriodIdx
  -> ChainState
mergeChainStateAt cs1 cs2 mergePointPeriod
  = let
      mergePoint = fromIntegralSafe $ mergePointPeriod * levelsPerPeriod
      leftSegment = V.take mergePoint cs1.currentLevels
      rightSegment = V.drop mergePoint cs2.currentLevels
      leftTailBlockHash = V.last leftSegment
      rightHeadBlockHash = V.head rightSegment
      rightHeadBlock = fromJust $ Map.lookup rightHeadBlockHash cs2.currentBlocks
      newRightHeadBlock = let
        RawBlock {..} = rightHeadBlock
        in RawBlock { header = rightHeadBlock.header { predecessor = leftTailBlockHash}, .. }
    in ChainState
      { currentBlocks =
          Map.insert rightHeadBlockHash newRightHeadBlock
            (Map.union cs1.currentBlocks cs2.currentBlocks)
      , currentLevels = leftSegment V.++ rightSegment
      , periodVoters =
          Map.union
            (Map.filterWithKey (\k _ -> k < mergePointPeriod) cs1.periodVoters)
            (Map.filterWithKey (\k _ -> k >= mergePointPeriod) cs2.periodVoters)
      , currentTime = cs1.currentTime
      , contextMap =
          Map.union
            (Map.filterWithKey (\k _ -> fromIntegralSafe (unLevel k) < mergePoint) cs1.contextMap)
            (Map.filterWithKey (\k _ -> fromIntegralSafe (unLevel k) >= mergePoint) cs2.contextMap)
      }

constants :: Constants
constants = Constants
  { blocksPerCycle = Just blocksPerCycle'
  , blocksPerVotingPeriod = Nothing
  , cyclesPerVotingPeriod = Just 5
  , timeBetweenBlocks = Nothing
  , minimalBlockDelay = Just $ MinimalBlockDelay 30
  , tokensPerRoll = Nothing
  , minimalStake = Just $ NumericString 6000000000
  }

rawConstants :: RawConstants
rawConstants = RawConstants $ toJSON constants

-- | Constants with blocksPerVotingPeriod instead of blocksPerCycle
rawConstants' :: RawConstants
rawConstants' = RawConstants $ toJSON (constants { blocksPerCycle = Nothing, blocksPerVotingPeriod = Just 320 })

type ChainStateRef = TVar ChainState

makeRandomSum :: VotingPower -> IO [VotingPower]
makeRandomSum 0 = pure []
makeRandomSum t = do
  randomSlice <- randomRIO (1, unVotingPower t)
  rest <- makeRandomSum $ VotingPower (unVotingPower t - randomSlice)
  pure $ VotingPower randomSlice : rest

mainChainId :: ChainId
mainChainId = ChainId "MainChain"

defaultProtocolHash :: ProtocolHash
defaultProtocolHash = Hash "Protocol1"

generateBlock
  :: UTCTime
  -> Level
  -> PeriodIdx
  -> Node.VotingPeriodKind
  -> Maybe BlockHash
  -> IO RawBlock
generateBlock blkTime level currentPeriod currentPeriodType mPredecessorHash = do
  bhSuffix <- randomRIO @Integer (0x1000000000000000, 0xFFFFFFFFFFFFFFFF)
  pure Node.RawBlock
    { hash = Node.Hash $ "Block" <> showt bhSuffix
      , chain_id = mainChainId
        , protocol = defaultProtocolHash
        , header = Header
        { level = level
          , predecessor = fromMaybe genesisBlockHash mPredecessorHash
            , timestamp = blkTime
            , proto = 0
        }
      , metadata = Just $ Node.RMDCurrent $ RawMetadataCurrent
      { voting_period_info = VotingPeriod $
          VotingPeriodInfo (unPeriodIdx currentPeriod) currentPeriodType
      }
      , operations = []
    }

mkRandomVoterWithVp :: VotingPower -> IO RawVoter
mkRandomVoterWithVp vp =
  randomIO @Word >>= \s -> pure $
    RawVoter (Hash $ pack $ "Voter" <> show s) Nothing (Just vp)

mkProposalOperationsAndVoters
  :: PeriodIdx
  -> (ProposalHash, VotingPower)
  -> IO [(RawOperation, RawVoter)]
mkProposalOperationsAndVoters periodIdx (phash, vp) = do
  vpComponents <- makeRandomSum vp
  forM vpComponents $ \vp' -> do
    randomVoter <- mkRandomVoterWithVp vp'
    operation <- mkProposalOperation periodIdx phash randomVoter
    pure (operation, randomVoter)

mkBallotOperationsAndVoters
  :: PeriodIdx
  -> ProposalHash
  -> (Ballot, VotingPower)
  -> IO [(RawOperation, RawVoter)]
mkBallotOperationsAndVoters periodIdx proposalHash (ballot, vp) = do
  vpComponents <- makeRandomSum vp
  forM vpComponents $ \vp' -> do
    randomVoter <- mkRandomVoterWithVp vp'
    operation <- mkBallotOperation periodIdx proposalHash randomVoter ballot
    pure (operation, randomVoter)

generateChain :: [PeriodConfig] -> IO ChainState
generateChain pconfigs =
  foldM generateBlocksForConfig emptyChainState pconfigs

generateBlocksForConfig :: ChainState -> PeriodConfig -> IO ChainState
generateBlocksForConfig cs pc = case pc of
  ProposalPeriod ppc -> do
    newCS <- (forM ppc.proposals (mkProposalOperationsAndVoters ppc.periodIdx)) >>=
      fillInOpsAndGenerateBlock
        ppc.startLevel ppc.numberOfBlocks ppc.periodIdx
          Proposal ppc.totalVotingPower cs
    pure $ newCS { contextMap = Map.insert ppc.startLevel ppc.protocolConstants newCS.contextMap }

  ExplorationPeriod epc -> do
    ballotsAndVoters :: [(RawOperation, RawVoter)] <-
      concat <$> mapM (mkBallotOperationsAndVoters epc.periodIdx epc.proposal)
        [(Yay, epc.ballots.yayBallots)
            , (Nay, epc.ballots.nayBallots), (Pass, epc.ballots.passBallots)]
    fillInOpsAndGenerateBlock epc.startLevel
      epc.numberOfBlocks epc.periodIdx Exploration epc.totalVotingPower cs [ballotsAndVoters]

  TestingPeriod tpc -> do
    foldM (\cs' level -> do
        (block, cs'') <- mkBlock cs' level tpc.periodIdx Node.Testing
        pure $ insertBlock cs'' block)
      cs [(tpc.startLevel)..(computeLastBlock tpc.startLevel tpc.numberOfBlocks)]

  PromotionPeriod ppc -> do
    ballotsAndVoters :: [(RawOperation, RawVoter)] <- liftIO $
      concat <$> mapM (mkBallotOperationsAndVoters ppc.periodIdx ppc.proposal)
        [ (Yay, ppc.ballots.yayBallots)
        , (Nay, ppc.ballots.nayBallots), (Pass, ppc.ballots.passBallots)]
    fillInOpsAndGenerateBlock ppc.startLevel ppc.numberOfBlocks
      ppc.periodIdx Promotion ppc.totalVotingPower cs [ballotsAndVoters]

  AdoptionPeriod apc -> do
    foldM (\cs' level -> do
        (block, cs'') <- mkBlock cs' level apc.periodIdx Node.Adoption
        pure $ insertBlock cs'' block)
          cs [apc.startLevel..(computeLastBlock apc.startLevel apc.numberOfBlocks)]
  where
    computeLastBlock :: Level -> Level -> Level
    computeLastBlock l numberOfBlocks = l + numberOfBlocks - 1

    -- Given a start/end levels, period index, voting period type, an aggregate voting power, and a list of operations
    -- and associated voters, generate the required number of blocks for the given period and period type, distributing
    -- the operations among them, and updates the voter map using the list of voters from the provided list,
    -- mutating the referenced chain state.
    fillInOpsAndGenerateBlock
      :: Level
      -> Level
      -> PeriodIdx
      -> VotingPeriodKind
      -> VotingPower
      -> ChainState
      -> [[(RawOperation, RawVoter)]]
      -> IO ChainState
    fillInOpsAndGenerateBlock startLevel numberOfBlocks periodIdx
      periodKind totalRequiredVotingPower chainState opsAndVoters = do
      let rawOperationsLists :: [[RawOperation]] =  do
            x <- opsAndVoters
            pure (fst <$> x)
      let voters :: [RawVoter] = concat $ do
            x <- opsAndVoters
            pure (snd <$> x)
      let
        fillInVotingPower = totalRequiredVotingPower -
          Prelude.sum (fromJust . (.voting_power) <$> voters)
      when (fillInVotingPower < 0) $
        throwRE ("Total voting power should be more than or equal to the\
          \ voting powers of the proposals: total voting power = " <> showt totalRequiredVotingPower)
      fillInVotingpowers <- liftIO $ makeRandomSum fillInVotingPower
      fillInVoters <- liftIO $ forM fillInVotingpowers mkRandomVoterWithVp
      let
        allVoters = voters <> fillInVoters
        allVotersMap = Map.fromList $ (\v -> (v.pkh, v)) <$> allVoters
        levelsWithOperations =
          zip
            [startLevel..(computeLastBlock startLevel numberOfBlocks)]
            ([[]] <> rawOperationsLists <> repeat []) -- Fill the rest of levels with empty operation lists

      let
        cs1 = chainState { periodVoters = Map.insert periodIdx allVotersMap cs.periodVoters }
      foldM (\cs' (level, ops) -> do
          (RawBlock {..}, csNew) <- mkBlock cs' level periodIdx periodKind
          pure $ insertBlock csNew $ RawBlock { operations = [ops :: [RawOperation]], .. }) cs1 levelsWithOperations

    mkBlock :: ChainState -> Level -> PeriodIdx -> VotingPeriodKind -> IO (RawBlock, ChainState)
    mkBlock cs' level periodIdx periodType = do
      let blkTime = cs'.currentTime
      b <- generateBlock blkTime level periodIdx periodType (getLastBlock cs')
      pure (b, incCurrentTime cs')

    insertBlock cs' block = cs'
      { currentLevels = V.snoc cs'.currentLevels block.hash
      , currentBlocks = Map.insert block.hash block cs'.currentBlocks
      }

    incCurrentTime cs' = cs' { currentTime = addUTCTime 30 cs'.currentTime }

getLastBlock :: ChainState -> Maybe BlockHash
getLastBlock cs = do
  if V.null cs.currentLevels then Nothing else Just $ V.last cs.currentLevels

levelsPerPeriod :: Num a => a
levelsPerPeriod = blocksPerCycle' * 5

blocksPerCycle' :: Num a => a
blocksPerCycle' = 64 -- 8192

mkProposalOperation :: PeriodIdx -> ProposalHash -> RawVoter -> IO RawOperation
mkProposalOperation periodIdx propHash voter = do
  operationHashSuffix1 :: Word <- randomIO
  pure Node.RawOperationRaw
        { contents = [Node.RawOperationContent
            { kind = "proposals"
            , proposals = Just [propHash]
            , proposal = Nothing
            , ballot = Nothing
            , source = Just voter.pkh
            , period = Just periodIdx
            }]
        , hash = Node.Hash $ pack $ "Operation" <> show operationHashSuffix1
        }

mkBallotOperation :: PeriodIdx -> ProposalHash -> RawVoter -> Ballot -> IO RawOperation
mkBallotOperation periodIdx hash voter ballot = do
  operationHashSuffix1 :: Word <- randomIO
  pure Node.RawOperationRaw
        { contents = [Node.RawOperationContent
            { kind = "ballot"
            , proposals = Nothing
            , proposal = Just hash
            , ballot = Just ballot
            , source = Just voter.pkh
            , period = Just periodIdx
            }]
        , hash = Node.Hash $ pack $ "Operation" <> show operationHashSuffix1
        }

data PeriodConfig
  = ProposalPeriod ProposalPeriodConfig
  | ExplorationPeriod ExplorationPeriodConfig
  | TestingPeriod TestingPeriodConfig
  | PromotionPeriod PromotionPeriodConfig
  | AdoptionPeriod AdoptionPeriodConfig

data BallotsConfig = BallotsConfig
  { yayBallots :: VotingPower
  , nayBallots :: VotingPower
  , passBallots :: VotingPower
  }

data ProposalPeriodConfig = ProposalPeriodConfig
  { startLevel :: Level
  , numberOfBlocks :: Level
  , periodIdx :: PeriodIdx
  , proposals :: [(ProposalHash, VotingPower)]
  , totalVotingPower :: VotingPower
  , protocolConstants :: RawConstants
  }

emptyConstants :: RawConstants
emptyConstants = RawConstants (Object mempty)

data ExplorationPeriodConfig = ExplorationPeriodConfig
  { startLevel :: Level
  , numberOfBlocks :: Level
  , periodIdx :: PeriodIdx
  , proposal :: ProposalHash
  , ballots :: BallotsConfig
  , totalVotingPower :: VotingPower
  }

data TestingPeriodConfig = TestingPeriodConfig
  { startLevel :: Level
  , numberOfBlocks :: Level
  , periodIdx :: PeriodIdx
  }

data PromotionPeriodConfig = PromotionPeriodConfig
  { startLevel :: Level
  , numberOfBlocks :: Level
  , periodIdx :: PeriodIdx
  , ballots :: BallotsConfig
  , proposal :: ProposalHash
  , totalVotingPower :: VotingPower
  }

data AdoptionPeriodConfig = AdoptionPeriodConfig
  { startLevel :: Level
  , numberOfBlocks :: Level
  , periodIdx :: PeriodIdx
  }

atomicReadChainState :: ChainStateRef -> IO ChainState
atomicReadChainState csRef = do
  liftIO $ readTVarIO csRef

startBlockForPeriod :: PeriodIdx -> Level
startBlockForPeriod (unPeriodIdx -> p) = Level $ 1 + (levelsPerPeriod * p)
