-- SPDX-FileCopyrightText: 2023 Tezos Commons
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
{-# LANGUAGE NumericUnderscores #-}

module Indexer.IndexerSpec
  ( spec
  ) where

import Control.Concurrent (threadDelay)
import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TVar
import Control.Concurrent.STM.TMVar (newTMVarIO)
import Control.Monad.Catch (MonadMask, bracket, handle, finally, throwM)
import Control.Monad.Reader
import Data.Map.Strict qualified as Map
import Data.Maybe
import Data.List qualified as DL
import Data.String (IsString)
import Data.Text (Text, pack)
import Data.Vector qualified as V
import Monad.Capabilities
import Servant.Client.Core (BaseUrl(..), Scheme(Http))
import System.Process
  (CreateProcess(..), StdStream(..), cleanupProcess, createProcess, interruptProcessGroupOf, proc,
  waitForProcess, getProcessExitCode)
import System.Random (randomRIO)
import Test.Hspec (Expectation, Spec, describe, it, shouldBe, expectationFailure)
import UnliftIO.Async (async, link, uninterruptibleCancel)
import GHC.Stack (HasCallStack)

import Api.Handlers qualified as AH
import Api.Types qualified as AT
import Caps
import Common
import Database.Caps
import Database.Models.Blocks
import DataStore qualified as Ds
import Indexer.ChainGenerator
import Logging
import Node
import Sync
import ServerPool

-- The tests here mostly end-to-end, and are implemented using a dummy block
-- chain generator and a mock "node" capability that serves a subset of tezos
-- node rpc using this generated chain.
--
-- The chain generation can be configured by providing Proposals and aggregate
-- vote counts, and the generator automatically generates a random list of
-- voters who's voting power add up to the provided aggregate voting power.
-- These proposal operations/ballot operations are distributed across the
-- blocks in the generated period.
--
-- Then we test the indexer api to check that the indexer counts agree with the
-- parameters that we provided to the chain generator.

spec :: Spec
spec =
  describe "Indexer behavior" $ do
    it "can do basic indexing and handle block chain forks" $ do
      let
        chainConfig =
          [ ProposalPeriod $ ProposalPeriodConfig
              (startBlockForPeriod 0 + 2) (levelsPerPeriod - 2)
              -- because the starting state of the chain already contains couple of blocks.
              0 [("Proposal1", 1000_0000), ("Proposal2", 500_000)] 2900_0000 rawConstants
          , ExplorationPeriod $ ExplorationPeriodConfig (startBlockForPeriod 1)
              levelsPerPeriod 1 "Proposal1" (BallotsConfig 2000_0000 1340_0000 200_0000) 7500_0000
          , ProposalPeriod $ ProposalPeriodConfig (startBlockForPeriod 2)
              levelsPerPeriod  2 [("Proposal3", 1000_0000), ("Proposal4", 500_000)] 2900_0000 rawConstants
          , ExplorationPeriod $ ExplorationPeriodConfig (startBlockForPeriod 3)
              levelsPerPeriod 3 "Proposal3" (BallotsConfig 2000_0000 1340_0000 200_0000) 7600_0000
          , TestingPeriod $ TestingPeriodConfig (startBlockForPeriod 4)
              levelsPerPeriod 4
          , PromotionPeriod $ PromotionPeriodConfig (startBlockForPeriod 5)
              levelsPerPeriod 5 (BallotsConfig 2000_0000 1340_0000 200_0000) "Proposal3" 7700_0000
          , AdoptionPeriod $ AdoptionPeriodConfig (startBlockForPeriod 6) levelsPerPeriod 6
          , ProposalPeriod $ ProposalPeriodConfig (startBlockForPeriod 7) levelsPerPeriod 7
              [("Proposal1", 1000_0000), ("Proposal2", 500_000)] 2900_0000 rawConstants
          ]

        chainConfigAlt =
          [ ProposalPeriod $ ProposalPeriodConfig (startBlockForPeriod 0 + 2)
              (levelsPerPeriod - 2) 0 [("Proposal1", 1000_0000), ("Proposal2", 500_000)] 2900_0000 rawConstants
          , ExplorationPeriod $ ExplorationPeriodConfig (startBlockForPeriod 1)
              levelsPerPeriod 1 "Proposal1" (BallotsConfig 2000_0000 1340_0000 200_0000) 7500_0000
          , ProposalPeriod $ ProposalPeriodConfig (startBlockForPeriod 2) levelsPerPeriod
              2 [("Proposal3", 1000_0005), ("Proposal4", 500_000)] 2900_0000 rawConstants
          , ExplorationPeriod $ ExplorationPeriodConfig (startBlockForPeriod 3) levelsPerPeriod 3
              "Proposal3" (BallotsConfig 2000_0000 1340_0000 200_0000) 7600_0000
          , TestingPeriod $ TestingPeriodConfig (startBlockForPeriod 4) levelsPerPeriod 4
          , PromotionPeriod $ PromotionPeriodConfig (startBlockForPeriod 5) levelsPerPeriod 5
              (BallotsConfig 2000_0000 1340_0000 200_0000) "Proposal3" 7700_0000
          , AdoptionPeriod $ AdoptionPeriodConfig (startBlockForPeriod 6) levelsPerPeriod 6
          , ProposalPeriod $ ProposalPeriodConfig (startBlockForPeriod 7) levelsPerPeriod 7
              [("Proposal1", 1000_0000), ("Proposal2", 500_000)] 2900_0000 rawConstants
          ]
      -- We generate two slightly different chains to test block chain forks.
      cs <- generateChain chainConfig
      csAlt <- generateChain chainConfigAlt

      testWithChainConfig (cs, mergeChainStateAt cs csAlt 1) $ \altChainSelRef cutOffRef -> do
        liftIO $ atomically $ writeTVar cutOffRef (startBlockForPeriod 6)
        waitForIndexerTill (startBlockForPeriod 6)
        let
          checkGetLevel p = do
            let
              sb = startBlockForPeriod p
              in AH.getLevel sb >>= \block -> liftIO $ block.level `shouldBe` sb
        -- Check api returns blocks at the required level.
        forM_ [1, 2, 3, 4, 5, 6] checkGetLevel

        -- Check it has indexed the proposals for a period.
        forM_ [1..4::Int] $ \x -> do
          let pHash = Hash $ "Proposal" <> pack (show x)
          proposal <- AH.getProposal pHash
          liftIO $ proposal.hash `shouldBe` pHash

        -- Check it has correctly indexed all the proposal operations for proposal/period combination.
        proposalOperations <- AH.getProposalOperations
            (Just $ Hash "Proposal1") (Just $ PeriodIdx 0) (Just 0) Nothing
        let totalVotingPower = sum $ (.votingPower) <$> proposalOperations
        liftIO $ totalVotingPower `shouldBe` 1000_0000

        -- Check it has correctly indexed all the ballot operations for proposal/period/ballot combination.
        ballotOperations <- AH.getBallots (Just $ Hash "Proposal1")
          (Just $ PeriodIdx 1) (Just 0) Nothing
        let sumBallotsFiltered blt =
              sum $ (.votingPower) <$> filter ((== blt) . (.vote)) ballotOperations
        liftIO $ do
          sumBallotsFiltered Yay `shouldBe` 2000_0000
          sumBallotsFiltered Nay `shouldBe` 1340_0000
          sumBallotsFiltered Pass `shouldBe` 200_0000

        -- To test blockchain fork handling, swap the node to use the alternate chain state with
        -- a slightly different configuration.
        liftIO $ atomically $ do
          writeTVar altChainSelRef True
          writeTVar cutOffRef (startBlockForPeriod 7)
        waitForIndexerTill (startBlockForPeriod 7)

        proposalOperations' <- AH.getProposalOperations (Just $ Hash "Proposal3")
          (Just $ PeriodIdx 2) (Just 0) Nothing
        let totalVotingPower' = sum $ (.votingPower) <$> proposalOperations'
        liftIO $ totalVotingPower' `shouldBe` 1000_0005
    it "marks proposal status as 'active' only for the winning proposal in a proposal period" $ do
      let
        chainConfig =
          [ ProposalPeriod $ ProposalPeriodConfig
              (startBlockForPeriod 0 + 2) (levelsPerPeriod - 2)
              -- because the starting state of the chain already contains couple of blocks.
              0 [("Proposal1", 1000_0000), ("Proposal2", 500_000)] 2900_0000 rawConstants
          , ExplorationPeriod $ ExplorationPeriodConfig (startBlockForPeriod 1) levelsPerPeriod 1
            "Proposal1" (BallotsConfig 2000_0000 11340_0000 200_0000)
              (2000_0000 + 11340_0000 + 200_0000 + 1_000)
          ]
      cs <- generateChain chainConfig
      testWithChainConfig (cs, emptyChainState) $ \_ cutOffRef -> do
        liftIO $ atomically $ writeTVar cutOffRef (startBlockForPeriod 3)
        waitForIndexerTill (startBlockForPeriod 2 - 10)
        [p1, p2] <- mapM AH.getProposal ["Proposal1", "Proposal2"]
        liftIO $ do
          p1.status `shouldBe` AT.Active
          p2.status `shouldBe` AT.Skipped
    it "marks proposal status as 'rejected' when an epoch does not go all the way" $ do
      let
        chainConfig =
          [ ProposalPeriod $ ProposalPeriodConfig
              (startBlockForPeriod 0 + 2) (levelsPerPeriod - 2)
              -- because the starting state of the chain already contains couple of blocks.
              0 [("Proposal1", 1000_0000), ("Proposal2", 500_000)] 2900_0000 rawConstants
          , ExplorationPeriod $ ExplorationPeriodConfig (startBlockForPeriod 1) levelsPerPeriod 1
              "Proposal1" (BallotsConfig 2000_0000 11340_0000 200_0000)
                (2000_0000 + 11340_0000 + 200_0000 + 1_000)
          , ProposalPeriod $ ProposalPeriodConfig
              (startBlockForPeriod 2) levelsPerPeriod
              -- because the starting state of the chain already contains couple of blocks.
              2 [] 2900_0000 rawConstants
          ]
      cs <- generateChain chainConfig
      testWithChainConfig (cs, emptyChainState) $ \_ cutOffRef -> do
        liftIO $ atomically $ writeTVar cutOffRef (startBlockForPeriod 6)
        waitForIndexerTill (startBlockForPeriod 3 - 10)
        proposal <- AH.getProposal "Proposal1"
        liftIO $ proposal.status `shouldBe` AT.Rejected
    it "Constants can be parsed from raw constants format in different protocols." $
      forM_ [rawConstants, rawConstants'] $ \cnsts -> do
        let
          chainConfig =
            [ ProposalPeriod $ ProposalPeriodConfig
                (startBlockForPeriod 0 + 2) (levelsPerPeriod - 2)
                -- because the starting state of the chain already contains couple of blocks.
                0 [("Proposal1", 1000_0000), ("Proposal2", 500_000)] 2900_0000 cnsts
            , ExplorationPeriod $ ExplorationPeriodConfig (startBlockForPeriod 1) levelsPerPeriod 1
              "Proposal1" (BallotsConfig 2000_0000 11340_0000 200_0000)
                (2000_0000 + 11340_0000 + 200_0000 + 1_000)
            ]
        cs <- generateChain chainConfig
        testWithChainConfig (cs, emptyChainState) $ \_ cutOffRef -> do
          liftIO $ atomically $ writeTVar cutOffRef (startBlockForPeriod 3)
          waitForIndexerTill (startBlockForPeriod 2 - 10)
          [pr] <- AH.getProtocols Nothing Nothing
          liftIO $ (pr.constants.blocksPerVoting) `shouldBe` Just 320
    it "Can run with an actual node" $ do
      let extraArgs =
            [ "--vote-exploration", "yay"
            , "--vote-promotion", "yay"
            ]

      let proto_hash :: IsString a => a
          proto_hash = "PsQuebecnLByd3JwTiGadoG4nGWi3HYiLXUjkibeFV8dCFeVMUg"

      -- this goes from Quebec to Quebec, which is a bit silly, but tests that
      -- indexer works with Quebec okay.
      withVotingScript proto_hash proto_hash extraArgs $ \port -> do
        testWithRealNode port $ do
          waitForIndexerTill 50
          [pr] <- AH.getProtocols Nothing Nothing
          proposal <- AH.getProposal proto_hash
          ballotsForPeriod2 <- AH.getBallots (Just proto_hash) (Just 2) Nothing Nothing
          ballotsForPeriod4 <- AH.getBallots (Just proto_hash) (Just 2) Nothing Nothing
          proposalOperations <- AH.getProposalOperations (Just proto_hash) Nothing Nothing Nothing
          liftIO $ do
            pr.hash `shouldBe` Hash proto_hash
            proposal.status `shouldBe` AT.Accepted
            length ballotsForPeriod2 `shouldBe` 2
            length ballotsForPeriod4 `shouldBe` 2
            all (== Yay) (AT.vote <$> ballotsForPeriod2) `shouldBe` True
            length proposalOperations `shouldBe` 1

      let current_proto_hash :: IsString a => a
          current_proto_hash = "PsParisCZo7KAh1Z1smVd9ZMZ1HHn5gkzbM94V3PLCpknFWhUAi"

      let next_proto_hash :: IsString a => a
          next_proto_hash = "PsQuebecnLByd3JwTiGadoG4nGWi3HYiLXUjkibeFV8dCFeVMUg"

      withVotingScript current_proto_hash next_proto_hash extraArgs $ \port -> do
        testWithRealNode port $ do
          waitForIndexerTill 50
          (base_proto : new_proto : _) <- AH.getProtocols Nothing Nothing
          proposal <- AH.getProposal next_proto_hash
          ballotsForPeriod2 <- AH.getBallots (Just next_proto_hash) (Just 2) Nothing Nothing
          ballotsForPeriod4 <- AH.getBallots (Just next_proto_hash) (Just 2) Nothing Nothing
          proposalOperations <- AH.getProposalOperations (Just next_proto_hash) Nothing Nothing Nothing
          liftIO $ do
            base_proto.hash `shouldBe` Hash current_proto_hash
            new_proto.hash `shouldBe` Hash next_proto_hash
            proposal.status `shouldBe` AT.Accepted
            length ballotsForPeriod2 `shouldBe` 2
            length ballotsForPeriod4 `shouldBe` 2
            all (== Yay) (AT.vote <$> ballotsForPeriod2) `shouldBe` True
            length proposalOperations `shouldBe` 1

withVotingScript
  :: (HasCallStack, MonadMask m, MonadIO m)
  => String -> String -> [String] -> (Int -> m a) -> m a
withVotingScript baseProtocol newProtocol extraArgs action = do
  -- to avoid different runners fighting for ports, use a random port.
  -- there's a chance it'll still fail on occasion, but what can you do.
  port <- randomRIO (16_384, 65_535)
  let procSpec = (proc "python3" $
        [ "./scripts/voting.py"
        , "--base-protocol", baseProtocol
        , "--new-protocol", newProtocol
        -- will run about 10 minutes unless killed. This is a safety net against
        -- leaving the node running indefinitely.
        , "--continue-after-activation", "120"
        , "--rpc-port", show port
        ] ++ extraArgs)
        { std_err = CreatePipe -- Hide Python's KeyboardInterrupt error message
        , create_group = True
          -- required for interruptProcessGroupOf (otherwise kills current process, too)
        }
  bracket
    (liftIO $ createProcess procSpec)
    (\args@(_, _, _, pid) -> liftIO $ do
        interruptProcessGroupOf pid -- ensures all children are killed too
        waitForProcess pid -- lets the process end normally
        cleanupProcess args -- cleans up iostreams
    )
    (\(_, _, _, pid) -> do
        let monitorProc = liftIO $ async $ do
              -- can't use waitForProcess as it locks up all IO without -threaded
              let poll = do
                    b <- isNothing <$> getProcessExitCode pid
                    threadDelay 1_000_000
                    when b poll
              poll
              expectationFailure "voting.py died prematurely"
        bracket monitorProc uninterruptibleCancel $ \mon -> do
          link mon
          action port
    )

testWithChainConfig
  :: (ChainState, ChainState)
  -> (TVar Bool -> TVar Level -> ApiM ())
  -> Expectation
testWithChainConfig (cs, csAlt) testFn = do
  -- These references are used to control the mock node capability
  -- to enable simulation of a blockchain fork.

  nodeCutOffRef <- newTVarIO 0 -- Block the node and thus the indexer initially
  --, and let the test decide when to start the node and the indexer.
  altChainSelRef <- newTVarIO False -- Use the default chain initially

  -- Initilaize the test database.
  runApiM dbInitialize

  -- We start a thread that runs the indexer
  indexer <- async $ withTestCapabilities (cs, csAlt)
    altChainSelRef nodeCutOffRef (runReaderT sync)
  runApiM $ do
      link indexer
      finally (testFn altChainSelRef nodeCutOffRef) Ds.clearDb

testWithRealNode
  :: HasCallStack
  => Int
  -> ApiM r
  -> IO r
testWithRealNode port testAction = do
    -- Initilaize the test database.
  runApiM dbInitialize

  -- We start a thread that runs the indexer
  indexer <- async $ withRealNodeTestCapabilities port (runReaderT sync)
  runApiM $ do
      link indexer
      finally testAction Ds.clearDb

-- Wait for the indexer to index upto the provided level.
waitForIndexerTill
  :: HasCap DbCap caps
  => Level
  -> CapsT caps IO ()
waitForIndexerTill l = do
  Ds.getLastCommitedBlock >>= \case
    Just lb -> if lb.level >= l then pass else do
      waitForIndexerTill'
    Nothing ->  waitForIndexerTill'
  where
    waitForIndexerTill' = do
      waitFor 1
      waitForIndexerTill l

testNodeCap
  :: TVar Bool
  -> TVar Level
  -> (ChainState, ChainState)
  -> CapImpl NodeCap '[NodeCap, LoggingCap] IO
testNodeCap altChainSelRef cutOffRef (cs, csAlt) = CapImpl $ NodeCap
  { _getBlock =
      \bs -> wrapHandler $ \cs' -> rawBlockToBlock <$> getBlockHandler cutOffRef cs' bs
  , _getMetadataForBlock =
      \t -> wrapHandler $ \cs' -> getBlockMetadata cutOffRef cs' (unHash t)
  , _getHeadBlockHeader = wrapHandler $ \cs' -> (.header) <$> getBlockHandler cutOffRef cs' NodeHead
  , _getVotersForBlock = \bs -> do
      c <- getConstantsForBlock bs
      wrapHandler $ \cs' -> do
        rvs <- getVoters cs' bs
        pure $ rawVoterToVoter (rawConstantsToConstants c) <$> rvs
  , _getQuorumForBlock = \t -> wrapHandler $ \_ -> Just <$> getQuorum (unHash t)
  , _getConstantsForBlock = \t -> wrapHandler $ \cs' -> getConstants cs' $ unHash t
  , _getStakingBalanceForVoter =  \_ _ -> error "Not implemented" -- We don't expect this to be called.
  }
  where
    -- Returns the reference to the default chain or the alternate chain
    -- depending on the value of altChainSelRef
    getCS :: IO ChainState
    getCS = readTVarIO altChainSelRef >>= \case
      True -> pure csAlt
      _ -> pure cs

    -- Convert the servant throwRE from the handlers to NodeCommunicationError,
    -- because that is the one that is handled in the indexer implementation.
    -- Any other exception will make it throw and giveup.
    wrapHandler :: forall m a. MonadIO m => (ChainState -> IO a) -> m a
    wrapHandler = liftIO . wrapHandler_
    wrapHandler_ :: forall a. (ChainState -> IO a) -> IO a
    wrapHandler_ act = do
      cs' <- getCS
      handle notFoundHandler (act cs')
      where
        notFoundHandler :: NotFoundError -> IO a
        notFoundHandler _ =  do
          waitFor 1
          wrapHandler_ act

withTestCapabilities
  :: (ChainState, ChainState)
  -> TVar Bool
  -> TVar Level
  -> (Capabilities SyncCaps IO -> IO a)
  -> IO a
withTestCapabilities chains altChainSelRef nodeCutOffRef fn = do
  emptyServers <- mkServerInfos []
  withDeps (\connection logger _ ->
    fn $ buildCaps $
      AddCap (testNodeCap altChainSelRef nodeCutOffRef chains) $
      AddCap (serverPoolCap emptyServers) $
      AddCap (loggingCap logger LogDebug) $
      AddCap (dbCap connection) $
      BaseCaps emptyCaps)

withRealNodeTestCapabilities
  :: Int
  -> (Capabilities SyncCaps IO -> IO a)
  -> IO a
withRealNodeTestCapabilities port fn = do
  localServer <- mkServerInfos
    [ BaseUrl
      { baseUrlScheme = Http
      , baseUrlHost = "127.0.0.1"
      , baseUrlPort = port
      , baseUrlPath = "/"
      }
    ]
  headBlockCache <- newTMVarIO Nothing
  withDeps (\connection logger _ ->
    fn $ buildCaps $
      AddCap (nodeCap headBlockCache) $
      AddCap (serverPoolCap localServer) $
      AddCap (loggingCap logger LogInfo) $
      AddCap (dbCap connection) $
      BaseCaps emptyCaps)

getBlockHandler
  :: TVar Level
  -> ChainState
  -> BlockSpec
  -> IO RawBlock
getBlockHandler cutOffRef gs bs = do
  let blockMap = currentBlocks gs
  let blockLevels = currentLevels gs
  cutOffLevel <- readTVarIO cutOffRef
  case bs of
    NodeHead ->
      case Map.lookup (V.last blockLevels) blockMap of
        Just rb -> if rb.header.level <= cutOffLevel
          then pure rb
          else throwM notFound
        Nothing -> throwM notFound
    HashSpec h ->
      case Map.lookup h blockMap of
        Just rb -> if rb.header.level <= cutOffLevel
          then pure rb
          else throwM notFound
        Nothing -> throwM notFound
    LevelSpec lvl@(Level l) ->
      if lvl <= cutOffLevel
        then case blockLevels V.!? fromIntegral l of
          Just x -> case Map.lookup x blockMap of
            Just rb -> pure rb
            Nothing -> throwM notFound
          Nothing -> throwM notFound
        else throwM notFound
    where
      notFound = NotFoundError "test error"

getBlockMetadata
  :: TVar Level
  -> ChainState
  -> Text
  -> IO RawMetadata
getBlockMetadata cutOffRef cs hash = do
  rb <- getBlockHandler cutOffRef cs (HashSpec $ Hash hash)
  pure $ fromMaybe (error "Metadata not available") $ rb.metadata

getPeriodForBlock
  :: RawBlock
  -> PeriodIdx
getPeriodForBlock rb = case rb.metadata of
  Just (RMDCurrent rmc) -> PeriodIdx $ rmc.voting_period_info.voting_period.index
  _ -> error "Period could not be determined for block"

getVoters
  :: ChainState
  -> BlockHash
  -> IO [RawVoter]
getVoters cs bh = do
  case Map.lookup bh cs.currentBlocks of
    Just rb -> case Map.lookup (getPeriodForBlock rb) cs.periodVoters of
      Just rvm -> pure $ Map.elems rvm
      _ -> pure []
    Nothing -> throwRE "Block not found"

getConstants
  :: ChainState
  -> Text
  -> IO RawConstants
getConstants cs blockhash = case Map.lookup (Hash blockhash) cs.currentBlocks of
  Just rb -> do
    let
      level = rb.header.level
    case DL.filter (\x -> (x <= level) || (x == 3 && level == 2)) $ Map.keys cs.contextMap of
      [] -> error $ "Empty context, this should not be possible:" <> show level <> (show cs.contextMap)
      x -> pure $ fromJust $ Map.lookup (maximum x) cs.contextMap
  Nothing -> error "Cannot find requested block"

getQuorum
  :: Text
  -> IO Quorum
getQuorum _ = pure (Quorum 60)
