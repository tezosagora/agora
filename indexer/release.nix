# SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
#
# SPDX-License-Identifier: AGPL-3.0-or-later

{ pkgs }: with pkgs;

let
  # Set a constant name for the src closure
  source = let
    root = ./.;
  in builtins.path {
    name = "agora-indexer-src";
    path = root;
    filter = name: type:
      (ignoreFilter name type && lib.cleanSourceFilter name type);
  };

  project = pkgs.callPackage ./. { };

  packages = [ project.agora-indexer.components.library ];
in
{
  inherit project;

  indexer-checks = with project; agora-indexer;
  indexer = symlinkJoin {
    name = "agora-indexer";
    paths = lib.attrValues project.agora-indexer.components.exes;
  };

}
