#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2022 Tezos Commons
#
# SPDX-License-Identifier: AGPL-3.0-or-later

files=$(git ls-files | xargs grep --files-with-matches --binary-files=without-match '[[:blank:]]$')
if [[ -n $files ]];then
  echo '  Files with trailing whitespace found:'
  for f in "${files[@]}"; do
    echo "  * $f"
  done
  exit 1
fi
