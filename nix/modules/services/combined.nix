# SPDX-FileCopyrightText: 2019-2021 Tocqueville Group
# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: AGPL-3.0-or-later

{ self }@args:

{ config, pkgs, lib, ... }:
let
  cfg = config.services.agora;
in {
  imports = [ (import ./frontend.nix args) (import ./backend.nix args) (import ./indexer.nix args) ];
  options.services.agora = {
    enable = lib.mkEnableOption "Agora frontend and backend combined with some sane defaults";
  };
  config = lib.mkIf cfg.enable {
    services.agora.frontend = {
      enable = true;
      api_addr = "http://${cfg.backend.config.api.listen_addr}";
      fqdn = lib.mkDefault "${config.networking.hostName}.${config.networking.domain}";
    };
    services.agora.backend = {
      enable = true;
      config.indexer_url = "http://127.0.0.1:${builtins.toString cfg.indexer.port}/v1";
    };
    services.agora.indexer = {
      enable = true;
      configurePostgres = true;
    };
    systemd.services.agora = rec {
      after = [ "agora-indexer-api.service" ];
      requires = after;
    };
  };
}
